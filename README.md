Docker DBVM
===========

This project wraps the MODIS direct-broadcast processing software, IMAPP Virtual Appliance - 
[Direct Broadcast Virtual Machine v?.? for Linux](http://cimss.ssec.wisc.edu/imapp/download/) (DBVM)
inside a [lightweight linux container](https://www.docker.io/the_whole_story/)
using [Docker](http://www.docker.io/) technology.
The DBVM download is a gzipped tar archive.
With Docker's explicit declarative requirement specification, it eliminates the pain
of chasing down the dependency libraries and greatly simplifies deployment.


Selected excerpts from the IMAPP Virtual Appliance v?.?
[User's Guide](ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/imapp_va/imapp_va_guide_1.1.pdf) (*link subject to change*).
Skip the section on *Installing the IMAPP Virtual Appliance* because
we will not run the software inside a conventional virtual machine.

> The processing system used by the IMAPP Virtual Appliance (known as the
"Direct Broadcast Virtual Machine" or DBVM) is implemented entirely in Bash
scripts (approx. 3000 lines). All of the Bash scripts are extensively commented, and are
designed to make it easy to add your own locally developed processing software.

> The DBVM processing system is designed to make it easy to change the
configuration of the processing packages. For example, if you want to create only
MODIS Level 1, Atmosphere (IMAPP), and Ocean (SeaDAS) products, the
system can be reconfigured in just a few minutes. The only part of the process
that takes time is downloading the processing packages from the Internet. The
DBVM installer script handles package downloads, installation, and
configuration. To add your own processing software to the system, you just need
to copy and modify one of the supplied processing scripts. Because the system is
easy to configure and install, there is no drawback to experimenting with
different system configurations. You can re-install the DBVM processing system,
or the IMAPP Virtual Appliance itself easily at any time.

> The DBVM processing system is designed to run automatically, and includes
optional scripts to ingest input data from either (a) a local disk, or (b) an FTP site.
Automation of data ingest is controlled by a single user-editable configuration
file. For example, to start ingesting input data from an FTP site, just edit two
lines in the configuration file, and then run one script. The DBVM processing
system recovers automatically following host system restarts and is ready to
process input data with no human intervention.

Even though DBVM may be configured to poll and download MODIS level-0 PDS files
from a remote HTTP or FTP server, this document assumes that the MODIS processing
server being set up will receive incoming PDS files via FTP instead.

Unless explicitly stated otherwise, every command given hereafter should be executed by
an ordinary user on the linux host.

Hardware requirements:

- 8 GB RAM
- 200 GB free disk space
- two 64-bit CPU cores

Software requirements:

- [linux host](https://www.docker.io/gettingstarted/#h_installation) that supports Docker
- [Git](http://git-scm.com/book/en/Getting-Started-Installing-Git)
- FTP server - not needed if DBVM is set up to poll and download from a remote server


## Prepare linux host

Ubuntu 12.04 vsftpd is used as example for the FTP server software setup.
An ordinary user on the linux host, *linuser*, will build and run the DBVM docker container.

As long as the linux host supports Docker, its distribution and version need not be
the same as the OS of the docker container.

1. Clone this project to a suitable directory
```
mkdir $HOME/projects
cd $HOME/projects
git clone https://cheewai@bitbucket.org/cheewai/docker-dbvm.git
```

1. Download packages required for DBVM installation totalling 15 GB. This step requires Internet connectivity.
```
mkdir $HOME/projects/docker-dbvm/tarfiles
cd $HOME/projects/docker-dbvm/tarfiles
# This step may take several hours
wget --input-file=$HOME/projects/docker-dbvm/main/downloads.txt
```

1. Superuser [installs Docker](https://www.docker.io/gettingstarted/#h_installation)

1. Superuser [installs git](http://git-scm.com/book/en/Getting-Started-Installing-Git)

1. Superuser adds *docker* as a supplementary group to linuser. linuser needs to re-login or start a new shell for usermod to take effect. Skip other steps in this section if you download PDS files from remote server.
```
usermod -a -G docker linuser
```

1. Superuser installs software for virtual FTP user and PAM authentication
```
apt-get install libpam-pwdfile vsftpd
```

1. Superuser creates FTP user, e.g. *ftpuser* 
```
mkdir /etc/vsftpd
perl -e 'print "ftpuser:",crypt("upload", "C0"),"\n"' >/etc/vsftpd/passwd
```

1. Superuser configures PAM authentication for vsftpd
```
cat <<EOT | sudo cat >/etc/pam.d/vsftpd
auth    required pam_pwdfile.so pwdfile /etc/vsftpd/passwd
account required pam_permit.so
EOT
```

1. Superuser configures vsftpd
```
mkdir -p /data/ftproot/linuser/modis
chmod 555 linuser.linuser /data/ftproot/linuser
chmod 755 linuser.linuser /data/ftproot/linuser/modis
chown -R linuser.linuser /data/ftproot/linuser
cp /home/linuser/projects/docker-dbvm/vsftpd.conf.example /etc/vsftpd.conf
service vsftpd restart
```


## Prepare Docker on linux host

This project has the following directory structure:

- home/ *(home directory inside Docker container)*
- main/ *(scripts and files for building and running Docker container)*
- products/ *(DBVM output)*



1. Create *main/build_run_env_nogit.sh* which defines some mandatory environment variables. This file is *sourced* by the *build* and *run* script. If this file is modified subsequently, the Docker container must be rebuilt. This file is explicitly specified in *.gitignore* and is therefore excluded from the Git repo.
```
REPO_NAME=docker-dbvm
REPO_TAG=0.1
APPUID=${SUDO_UID:-`id -u`}
APPUSER=*username in container*
APPPASS=*crypt hash for password, not used usually*
FTPUSER=ftpuser
FTPROOT=/data/ftproot
FTPHOME=$FTPROOT/linuser
ULDIR=modis
# Actual upload directory: $FTPROOT/$FTPUSER/$ULDIR/
```

1. Build the Docker container. This step requires Internet connectivity and typically takes more than 30 minutes.
```
cd $HOME/projects/docker-dbvm/main
./build
```



## Install DBVM

1. Run DBVM install script inside docker container
```
$HOME/projects/docker-dbvm/main/run bash
dbvm/scripts/install_dbvm.bash
# When prompted by install script, enter 'yes'
# When prompted to download, enter 'No'
# Logout from Docker container to return to linux host
```

## Customize DBVM

Select MODIS products by editing *$HOME/projects/docker-dbvm/home/dbvm/scripts/dbvm_env.bash*


## DBVM Operation

In normal operation, DBVM *daemon_pds_event.bash* runs in the background which looks for any file with **.event** extension in *dbvm/data/level0/*
as an indication of new data for processing.
For example, when it finds MODIS TERRA *P0420064AAAAAAAAAAAAAA14062091613000.PDS.event*, it will begin
processing *P0420064AAAAAAAAAAAAAA14062091613000.PDS*.

In our setup, incoming data are being uploaded via FTP to /data/ftproot/... in the linux host.
A separate background program, *dirmon.py*, monitors this directory for new files.
It copies new PDS files to DBVM level0 directory and creates the .event file.
*dirmon.py* relies on additional logic defined in *dirmon_action/modisraw.py* to handle
MODIS AQUA level0 files which come in pairs:

- *P1540957A\*001.PDS* [Aqua Ground Based Attitude Determination (GBAD)](http://directreadout.sci.gsfc.nasa.gov/?id=dspContent&cid=14)
- *P1540064A\*001.PDS* swath data

Only when a complete set is present will *dirmon.py* create the *P1540064A\*001.PDS.event* file to signal the DBVM daemon.


## Define product archive directory

Other than feeding incoming files to DBVM, *dirmon.py* is also used by copying files for archiving purposes.
Its default configuration file is *bin/dirmon.ini*. You can run different instances of *dirmon.py* by specifying a different configuration file to use.
The *.ini* extension is a misnomer. The file is actually in [JSON format](http://en.wikipedia.org/wiki/JSON).

Remember, any pathnames specified refers to the docker container filesystem namespace
and not that of the linux host.

In this minimal setup, new PDS files for both TERRA and AQUA will be sent to DBVM for processing.
```
{
  "/data/ftproot/ftpuser/modis": [
    {
      "include": "P154*001.PDS",
      "action": "/home/modisuser/dbvm/data/level0"
    },
    {
      "include": "P0420064AAAAAAAAAAAAAA*001.PDS",
      "action": "/home/modisuser/dbvm/data/level0" 
    }
  ],
  "/home/modisuser/dbvm/data/level0": "modisraw",
}
```

In this example, MOD04 and MOD35 products will be copied to */data/products/{mod04,mod35}* directories respectively.
Any new files in */home/modisuser/fireloc/name-scheme-three* matching the **include** pattern will cause
*/home/modisuser/bin/{make_fireloc.sh,make_truecolor.sh}* to be executed.
```
{
  "/home/modisuser/dbvm/data/level2": [
    {
      "include": "*.mod04.hdf",
      "action": "/data/products/mod04"
    },
    {
      "include": "*.mod35.hdf",
      "action": "/data/products/mod35"
    }
  ],
  "/home/modisuser/fireloc/name-scheme-three": [
    {
      "include": "MODIS*.txt",
      "action": "/home/modisuser/bin/make_fireloc.sh"
    },
    {
      "include": "MODIS*.txt",
      "action": "/home/modisuser/bin/make_truecolor.sh"
    }
  ]
}
```



## Customize cron schedule

One of the main purpose is to delete old files to free up disk space on the linux host.
Review and edit *$HOME/projects/docker-dbvm/main/crontab-user*
When the docker container is run, this file will be installed into */etc/cron.d/*.

Remember, any pathnames specified refers to the docker container filesystem namespace
and not that of the linux host.


## Finally,
On the linux host
```
$HOME/projects/docker-dbvm/main/run
```


## TODO
- startup.sh should apply locally-modified scripts
- Explain how to get MOD14 CSV
- Additional cookie-cut truecolor images
- How to install and run optional algorithms, burnscar, BRDF
- Alternative to DBVM using NASA IPOPP?
- Separate project for NPP processing

