#! /bin/bash
#
# Bootstrap script when container starts up
# CHANGES TO THIS FILE WILL BE OVERWRITTEN BY docker run
#
chown -R modisuser.modisuser /home/modisuser /data/products
crontab -u modisuser /main/crontab-dbvm
cp -f /main/crontab-user /etc/cron.d/modisuser
service cron restart
su modisuser -c '/home/modisuser/bin/dirmon.py -d --logfile=/var/tmp/dirmon.log'
tail -f /var/tmp/dirmon.log
