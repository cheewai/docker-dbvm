#! /bin/bash

#
# Run once per MODIS overpass from DBVM run_user.bash
# Take about 60 minutes to complete
#
export MIPS_INSTALL_DIR=$HOME/mips
export MIPS_HOME=$MIPS_INSTALL_DIR/mips.version.2.1.public
source $MIPS_HOME/scripts/environment/default/mips_modis_env.bash

if [ $# -lt 1 ] || [ ! -f "$1" ]; then
  echo "usage: $0 l1b_1km.hdf [...]" >&2
  exit 1
fi

tmp_dir=/var/tmp/$$
mkdir $tmp_dir && pushd $tmp_dir
if [ $? -ne 0 ]; then
  echo "mkdir/chdir $tmp_dir failed" >&2
  exit 1
fi

for l1b in $*; do
  rm -f *
  l1b_dir=$(dirname $l1b)
  l1b_name=$(basename $l1b)
  l1b_name=${l1b_name:0:13}
  if [ "${l1b_name:0:1}" = "t" ]; then
    pfx=MOD
    satname="terra"
  else
    pfx=MYD
    satname="aqua"
  fi
  jjj="${l1b_name:5:3}"
  yyyy="20${l1b_name:3:2}"
  hhmm="${l1b_name:9:4}"
  if [ "$hhmm" -le "0500" ] || [ "$hhmm" -gt "1700" ]; then
    echo "Skipped nighttime overpass: $l1b_name"
    continue
  fi
  yyyymmdd=$(date -d "${yyyy}-01-01 ${jjj} days" '+%Y%m%d')
  okm="$l1b_dir/$l1b_name.1000m.hdf"
  hkm="$l1b_dir/$l1b_name.500m.hdf"
  qkm="$l1b_dir/$l1b_name.250m.hdf"
  geo="$l1b_dir/$l1b_name.geo.hdf"
  $MIPS_HOME/scripts/process_granule_tiles_modis.bash $okm $hkm $qkm $geo $satname $yyyymmdd $hhmm
done
popd
rm -rf "$tmp_dir"
