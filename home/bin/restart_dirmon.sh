#! /bin/bash

file_dirmon=$HOME/bin/dirmon.py

if [ ! -x $file_dirmon ]; then
    echo "ERROR: dirmon script '$file_dirmon' does not exist or not executable" >&2
    exit 1
fi
pkill -f $file_dirmon
$file_dirmon -d --logfile=/var/tmp/dirmon.log
