#! /bin/bash
#
# Run MODIS Level 3 Burn Scar Science Processing Algorithm
#
# "This algorithm maps post-fire burned areas using daily composites of
# 500m MODIS corrected reflectance and daily composites of 1km MODIS
# active fire observations. It identifies the date of burn, to the nearest day,
# for 500m grid cells within individual MODIS Level 3 tiles."
#

set -u # Abort on unset environment variable

#######
#
# These parameters must match those in the daily-run script
#

# Tile list
tile_lst="h19v10 h19v11 h19v12 h20v10 h20v11 h20v12 h21v10"

# Input directory where CREFL and MOD14 daily composites are
#20131120 daily_dir=/data/NFS/lake/Modis/rsru-archive/Products/burnscar/daily_tiles
daily_dir=/mnt/poseidon/modis/products/burnscar/daily_tiles
#######
 
#######
#
# Default date parameters determined based on current date
#
if [ $# -eq 3 ]; then
  yyyy=$1
  jjjstart=$2
  jjjend=$3
elif [ $# -eq 0 ]; then
  yyyy=$(date +%Y)
  # Wanted month is 2 months ago
  mm=$(( $(date +%m) - 2))
  # If run in Jan/Feb, adjust to previous year
  if [ $mm -lt 1 ]; then
    let 'mm += 12'
    let 'yyyy -= 1'
  fi
  dd=$(date +%d)
  jjjstart=$(date -d "${mm}/1" +%j)
  jjjend=$(date -d "-1 month -${dd} days" +%j)
else
  cat >&2 <<END
usage: $0 [yyyy jjjstart jjjend]

Without arguments, the default period will be the
whole calendar month 2 months ago.
There should be at least 30 days of daily composites after jjjend
in daily_dir ($daily_dir)
END
  exit 1
fi

#echo $yyyy $jjjstart $jjjend
#######

# Run script
burnscar_prog=$HOME/SPA/burnscar/wrapper/burnscar/run

# Working directory
work_dir=/var/tmp/burnscar_monthly

# Output directory
#20131120 monthly_dir=/data/NFS/lake/Modis/rsru-archive/Products/burnscar/monthly_tiles
monthly_dir=/mnt/poseidon/modis/products/burnscar/monthly_tiles

[ -d "$work_dir" ] || mkdir "$work_dir"
cd "$work_dir"
if [ $? -ne 0 ]; then
  echo "Invalid work_dir: $workdir" >&2
  exit 1
fi


for tile in $tile_lst; do
  rm -f *
  ps_file=PRESCAR.${tile}.${yyyy}.${jjjstart}.${jjjend}.hdf
  bs_file=BURNSCAR.${tile}.${yyyy}.${jjjstart}.${jjjend}.hdf
  if [ -f "$monthly_dir/$ps_file" ] && [ -f "$monthly_dir/$bs_file" ]; then
    continue # output present, skip
  fi
  cmd=`cat <<EOF
  $burnscar_prog \
  inputpath $daily_dir \
  modis.prescar_tile $ps_file \
  modis.burnscar_tile $bs_file \
  year ${yyyy} \
  startday $jjjstart \
  endday $jjjend \
  tileID $tile
EOF
`
  $cmd
  rc=$?
  if [ $rc -eq 0 ]; then
    cp $ps_file $bs_file $monthly_dir
  else
    echo "ERROR $rc: $cmd\n" >&2
  fi
done
