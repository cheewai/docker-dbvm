#! /bin/bash

#
# Run once per day when no more daytime overpass is expected.
# Produces composited images for predefined ROIs
# Takes about 30 minutes to complete
#
export MIPS_INSTALL_DIR=$HOME/mips
export MIPS_HOME=$MIPS_INSTALL_DIR/mips.version.2.1.public
source $MIPS_HOME/scripts/environment/default/mips_modis_env.bash

tmp_dir=$(mktemp -d)
if [ $? -ne 0 ]; then
  echo "Can't mkdir/chdir $tmp_dir" >&2
  exit 1
fi
pushd $tmp_dir
$MIPS_HOME/scripts/process_subsets.bash
popd
rm -rf "$tmp_dir"
