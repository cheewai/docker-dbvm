import os, re

#
# In order to process MODIS AQUA, DBVM requires both P154*00[01].PDS files
# For TERRA, P046*001.PDS suffices.
# When triggered by new PDS files, this module checks if input files
# are all available, and creates a $DBVM_HOME/data/level0/P*.001.PDS.event
# to signal to DBVM daemon_pds_event.bash
#
terra_re = re.compile(r'P0420064A{14}(?P<timestamp>\d{11})(?P<one>\d{3})\.PDS$')
terra_fn = 'P0420064AAAAAAAAAAAAAA%(timestamp)s001.PDS'

data_re = re.compile(r'^P1540064A{14}(?P<timestamp>\d{11})(?P<one>\d{3})\.PDS$')
data_fn = 'P1540064AAAAAAAAAAAAAA%(timestamp)s001.PDS'

gbad_re = re.compile(r'^P1540957A{14}(?P<timestamp>\d{11})(?P<one>\d{3})\.PDS$')
gbad_fn = 'P1540957AAAAAAAAAAAAAA%(timestamp)s001.PDS'

def aqua_wanted(filter):
    if not filter or filter[0].upper() == 'A':
        return True
    else:
        return False

def terra_wanted(filter):
    if not filter or filter[0].upper() == 'T':
        return True
    else:
        return False

def ModisrawFileProcessor(dir, file, fullpath, logger, filter=None, **kwargs):
    '''MODIS AQUA data from EOS-FES is ready for processing only when both 
    P1540064A*001.PDS and P1540957A*001.PDS (GBAD) are present
    '''
    event_fn = None
    discard = True
    m = data_re.match(file)
    if m and m.group('one') == '001':
        if aqua_wanted(filter) and os.path.exists(os.path.join(dir, gbad_fn%m.groupdict())):
            discard = False
    else:
        m = gbad_re.match(file)
        if m and m.group('one') == '001':
            if aqua_wanted(filter) and os.path.exists(os.path.join(dir, data_fn%m.groupdict())):
                event_fn = data_fn%m.groupdict() + '.event'
                discard = False
        elif terra_wanted:
            m = terra_re.match(file)
            if m and m.group('one') == '001':
                discard = False
        
    if discard:
        logger.debug("%s discarded" % file)
        return False
    if event_fn is None:
        event_fn = file + '.event'
    if not os.path.exists(os.path.join(dir, event_fn)):
        fh = open(os.path.join(dir,event_fn), 'w+')
        fh.close()        
        logger.info("created event file %s" % event_fn)
    return True
