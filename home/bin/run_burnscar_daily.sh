#! /bin/bash
#
# Given yyjjj, make Burnscar SPA daily intermediate products
#

set -u # Abort on unset environment variable


crefl_dir=$HOME/apps/ipopp/SPA/burnscar/wrapper/creflhkm
tile_lst="h19v10 h19v11 h19v12 h20v10 h20v11 h20v12 h21v10"
mod14comp_dir=$HOME/apps/ipopp/SPA/burnscar/wrapper/mod14-composite
creflcomp_dir=$HOME/apps/ipopp/SPA/burnscar/wrapper/crefl-composite

l1_dir=/home/modisdbc0/dbvm/data/level1
l2_dir=/home/modisdbc0/dbvm/data/level2
tmp_dir=/var/tmp/burnscar/output

# Directory for daily MOD14 and CREFL tile products
#20131120 out_dir=/data/NFS/lake/Modis/rsru-archive/Products/burnscar/daily_tiles
out_dir=/mnt/poseidon/modis/products/burnscar/daily_tiles

#if [ $# -ne 1 ] || ! [[ $1 =~ ^[0-9]{5}$ ]] ; then
#    echo "usage: $0 yyjjj" >&2
#    exit 1
#fi
yyjjj=${1:-`date +'%y%j' -d '-1 day'`}

# Get list of MOD14 files
mod14_lst=$(ls $l2_dir/??.${yyjjj}.*.mod14.hdf)

#DRYRUN=echo
DRYRUN=
#cd $tmp_dir && rm -f *
cd $tmp_dir

mod14comp_arg=
creflcomp_arg=
i=1
for mod14_path in $mod14_lst ; do
    mod14_file=$(basename $mod14_path)
    prefix=${mod14_file%.mod14.hdf}
    crefl_path=$tmp_dir/$prefix.creflhkm.hdf
    mod03_path=$l1_dir/$prefix.geo.hdf
    mod14_path=$l2_dir/$prefix.mod14.hdf
    
    # Create CREFL half-km swath product
    creflqkm_path=$l1_dir/$prefix.250m.hdf
    creflhkm_path=$l1_dir/$prefix.500m.hdf
    crefl1km_path=$l1_dir/$prefix.1000m.hdf
    creflgeo_path=$l1_dir/$prefix.geo.hdf

    if [ ! -f $crefl_path ] && [ -f $creflqkm_path ] && [ -f $creflhkm_path ] && [ -f $crefl1km_path ]; then
        $DRYRUN $crefl_dir/run \
            modis.mxd02qkm $creflqkm_path \
            modis.mxd02hkm $creflhkm_path \
            modis.mxd021km $crefl1km_path \
            modis.creflhkm $crefl_path # output
        sleep 1
    fi
    if [ -f $crefl_path ] && [ -f $creflgeo_path ]; then
        # Build argument list for MOD14 compositor
        mod14comp_arg="$mod14comp_arg modis.mxd03_$i $mod03_path modis.firedetection_$i $mod14_path"
        creflcomp_arg="$creflcomp_arg modis.creflhkm_$i $crefl_path modis.mxd03_$i $mod03_path modis.firedetection_$i $mod14_path"
        let i=i+1
    else
        echo "Incomplete level1b $mod14_file: $crefl_path,$creflgeo_path" >&2
    fi
done

#echo "###"
#echo "$mod14comp_arg"
#echo "$creflcomp_arg"
#exit 0

for t in $tile_lst; do
    mod14tile_file=MXD14.$t.$yyjjj.hdf
    mod14tile_path=$tmp_dir/$mod14tile_file
    crefltile_file=MXDcreflhkm.$t.$yyjjj.hdf
    crefltile_path=$tmp_dir/$crefltile_file

    if [ -n "$mod14comp_arg" ] && [ ! -f $mod14tile_path ] && [ ! -f $out_dir/$mod14tile_file ] ; then
        # Create MOD14 daily composite tiled product
        $DRYRUN $mod14comp_dir/run $mod14comp_arg tileID $t modis.firedetection_tile $mod14tile_path
        $DRYRUN cp -f $mod14tile_path $out_dir
    fi
    if [ -n "$creflcomp_arg" ] && [ ! -f $crefltile_path ] && [ ! -f $out_dir/$crefltile_file ] ; then
        # Create CRELF half-km daily composite tiled product
        $DRYRUN $creflcomp_dir/run $creflcomp_arg tileID $t modis.creflhkm_tile $crefltile_path
        $DRYRUN cp -f $crefltile_path $out_dir
    fi
done
