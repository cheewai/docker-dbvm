#!/bin/bash

# Sample Script for User Defined Processing in DBVM
# Liam.Gumley@ssec.wisc.edu

echo
echo "Creating User Defined Products; started at "$(date -u)

# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_user.bash file_1km "
  echo "where"
  echo "file_1km is the full path and name of the input MODIS Level 1B 1KM HDF file"
  exit 1
fi

#------------------------------------------------------------------------------
# GET INFORMATION ABOUT THE SATELLITE NAME, DATE, AND TIME
#------------------------------------------------------------------------------

# Get name of MODIS 1KM file
file_1km=$1

# Get satellite name (terra or aqua)
sat_id=$(basename $file_1km | cut -c1-2)
if [ "$sat_id" == "t1" ]; then
  sat_name="terra"
else
  sat_name="aqua"
fi

# Get satellite/date/time (e.g., a1.06260.2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Get pass year, day of year, time (e.g, 06 260 2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
year=$(basename $file_1km | cut -c4-5)
jday=$(basename $file_1km | cut -c6-8)
time=$(basename $file_1km | cut -c10-13)

# Get names of MODIS HKM, QKM, and GEO files
file_hkm=${file_1km%.1000m.hdf}.500m.hdf
file_qkm=${file_1km%.1000m.hdf}.250m.hdf
file_geo=${file_1km%.1000m.hdf}.geo.hdf

#------------------------------------------------------------------------------
# START OF USER DEFINED COMMANDS
#------------------------------------------------------------------------------

# Include user directory in the PATH
export PATH=$DBVM_HOME/user:$PATH

# If daytime MODIS data exists, create regional true color images
if [ -e $file_hkm ]; then
  echo
  #echo "(Creating regional MODIS true color images)"
  #echo "(NOT creating regional MODIS true color images)"
  #modis_truecolor.bash $file_1km 41.52  -93.30 49.00  -84.81 Wisconsin
  #modis_truecolor.bash $file_1km 25.75 -106.75 36.75  -93.25 Texas
  #modis_truecolor.bash $file_1km 32.31 -124.57 42.13 -113.94 California
  echo "(Creating regional MODIS true color images)"
  /home/modisdbc0/bin/run_mips.sh $file_1km
fi

#------------------------------------------------------------------------------
# END OF USER DEFINED COMMANDS
#------------------------------------------------------------------------------

echo
echo "Finished User Defined Products at "$(date -u)

exit 0
