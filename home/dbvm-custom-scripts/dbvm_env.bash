# DBVM environment configuration file

# Path for scripts
export PATH=$DBVM_HOME/scripts:$PATH

# Base processing
export DBVM_LEVEL1="YES"			# MODIS Level 1B products
export DBVM_ATMOS="YES"				# MODIS Atmosphere products

# Optional processing 
export DBVM_DEM="YES"				# MODIS Geolocation terrain correction
export DBVM_IMAGE="YES"				# MODIS GeoTIFF and JPEG image products
export DBVM_DBGE="YES"				# MODIS Google Earth image products
export DBVM_LAND="YES"				# MODIS Land products (requires DBVM_DEM=YES)
export DBVM_MOD09="YES"				# MODIS Land Surface Reflectance products (requires DBVM_LAND=YES)
export DBVM_OCEAN="YES"				# MODIS Ocean products
export DBVM_AIRS="YES"				# AIRS products (Aqua only)
export DBVM_AMSRE="YES"				# AMSR-E products (Aqua only)
export DBVM_USER="YES"				# User-defined products

# Directory names
export ERROR_DIR=$DBVM_HOME/errors
export LOG_DIR=$DBVM_HOME/logs
export DATA_DIR=$DBVM_HOME/data
export IMAGE_DIR=$DATA_DIR/images
export L0_DIR=$DATA_DIR/level0
export L1_DIR=$DATA_DIR/level1
export L2_DIR=$DATA_DIR/level2
export WORK_DIR=$DATA_DIR/work

# Data ingest options
export DBVM_LOCAL_DATA="NO"
export DBVM_LOCAL_DIR="/data"
export DBVM_LOCAL_INTERVAL="60"
export DBVM_FTP_DATA="NO"
#CLAI export DBVM_FTP_DIR="ftp://ftp.ssec.wisc.edu/pub/eosdb/pds"
export DBVM_FTP_INTERVAL="300"

# Data retention time (days)
export DBVM_RETAIN="3"

# Data archive options
export DBVM_ARCHIVE="NO"
export DBVM_ARCHIVE_DIR="/archive"
export DBVM_HDF_COMPRESS="YES"

# Locations for leapsec.dat and utcpole.dat
# 20131008 US govt shutdown
# https://forums.ssec.wisc.edu/viewtopic.php?f=13&t=353
export DBVM_LEAPSEC="http://oceandata.sci.gsfc.nasa.gov/Ancillary/LUTs/modis/leapsec.dat"
export DBVM_UTCPOLE="http://oceandata.sci.gsfc.nasa.gov/Ancillary/LUTs/modis/utcpole.dat"
# 20131022 NASA web site back online
#export DBVM_LEAPSEC="ftp://ftp.ssec.wisc.edu/pub/gumley/leapsec_utcpole/leapsec.dat"
#export DBVM_UTCPOLE="ftp://ftp.ssec.wisc.edu/pub/gumley/leapsec_utcpole/utcpole.dat"
