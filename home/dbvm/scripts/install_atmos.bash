#!/bin/bash

# Install MODIS Atmosphere processing applications

echo
echo "Starting installation of MODIS Atmosphere processing applications, please wait..."

echo "(Installing IMAPP)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/IMAPP_MODISL2_V2.2.tar.gz

echo "(Installing IMAPP patches)"
rsync -aC $DBVM_HOME/scripts/patches/imapp_modisl2 $DBVM_HOME/apps

echo "(Installing MCLITE)"
cd $DBVM_HOME/apps/imapp_modisl2
tar zxf $DBVM_HOME/tarfiles/McLITE-linux-imapp-1.5.tar.gz

echo "Finished installation of MODIS Atmosphere processing applications"
exit 0
