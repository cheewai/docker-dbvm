export SEADAS=$DBVM_HOME/apps/seadas
export OCSSWROOT=$SEADAS
source $SEADAS/config/OCSSW_bash.env
export SDSTMP=/tmp
export SDS_USER_PATH=.
export SDSBIN=$SEADAS/run/bin
export SDSDEMO=$SEADAS/demo
export SDSHELP=$SEADAS/doc
export SDS_IDLLIB=$SEADAS/idl_lib
 
# Environment variables with 'Make Default' buttons
export L2GEN_ANC=$DATA_DIR/ancillary/seadas
export MODIS_ATTEPH=$SEADAS/run/var/modis/atteph
export AQUA_REFL_LUT=$SEADAS/run/var/modisa/cal/OPER/MYD02_Reflective_LUTs.V6.1.7.3_OCb.hdf
export AQUA_EMIS_LUT=$SEADAS/run/var/modisa/cal/OPER/MYD02_Emissive_LUTs.V6.1.7.3_OCb.hdf
export AQUA_QA_LUT=$SEADAS/run/var/modisa/cal/OPER/MYD02_QA_LUTs.V6.1.7.3_OCb.hdf
export TERRA_REFL_LUT=$SEADAS/run/var/modist/cal/OPER/MOD02_Reflective_LUTs.V6.1.6.2.hdf
export TERRA_EMIS_LUT=$SEADAS/run/var/modist/cal/OPER/MOD02_Emissive_LUTs.V6.1.6.2.hdf
export TERRA_QA_LUT=$SEADAS/run/var/modist/cal/OPER/MOD02_QA_LUTs.V6.1.6.2.hdf
export KEEP_MODIS_1KM=ON
export KEEP_MODIS_HKM=OFF
export KEEP_MODIS_QKM=OFF
export MODIS_TERRAIN=ON
export MODIS_DEFINITIVE_ATTEPH=ON
export MODIS_DEFINITIVE_FTP=ON
export MODIS_REALTIME_ATTEPH=ON
export MODIS_REALTIME_FTP=ON
export AUTO_MET=ON
export AUTO_OZONE=ON
export AUTO_SST_MODIS=ON
export AUTO_SST_OTHER=ON
export AUTO_NO2=OFF
export AUTO_ICE=ON
export FTP_VERBOSITY=OFF
export GEOCHECK_THRESHOLD=95
export STARTNUDGE=0
export STOPNUDGE=10
 
export SEADAS_BROWSER=Firefox
export SDSWFONT='-adobe-courier-bold-r-normal--12-120-75-75-m-70-iso8859-1'
 
if [ -f ~/.seadas/seadas.env_user_bash ]; then
   source ~/.seadas/seadas.env_user_bash
fi
 
export PATH=.:$SEADAS/run/scripts:$SEADAS/run/bin:$SEADAS/run/bin3:$SDS_USER_PATH:$PATH

