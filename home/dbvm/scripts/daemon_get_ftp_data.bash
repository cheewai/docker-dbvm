#!/bin/bash

# Script to ingest Level 0 MODIS data files from a remote FTP server

# NOTES:
# 1. This script is intended to run continuously in the background, i.e., $ daemon_get_ftp_data.bash &
# 2. This script must be restarted if it is changed
# 3. To restart the script, execute restart_daemon_scripts.bash

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$LOG_DIR/daemon_get_ftp_data.log
exec >>$LOG
exec 2>>$LOG

# Set remote FTP directory names
source_terra_dir=$DBVM_FTP_DIR
source_aqua_dir=$DBVM_FTP_DIR
source_gbad_dir=$DBVM_FTP_DIR
source_airs_dir=$DBVM_FTP_DIR

# Set local target directory name
target_dir=$L0_DIR

# Start infinite loop
while [ true ]; do

  # Get Terra and Aqua MODIS Level 0 construction record (CR) files (P???0064*00.PDS)
  # that were modified in the last 120 minutes
  lftp -c "mirror --newer-than=now-120minutes -r -c -I P0420064*00.PDS $source_terra_dir $target_dir"
  lftp -c "mirror --newer-than=now-120minutes -r -c -I P1540064*00.PDS $source_aqua_dir  $target_dir"
  cr_list=$(find $target_dir/ -name "P???0064*00.PDS" -mmin -120 | sort)

  # Loop over CR files
  for cr_file in $cr_list; do

    # Get MODIS Level 0 data file name (P???0064*01.PDS)
    cr_base=$(basename $cr_file)
    data_file=${cr_base%00.PDS}"01.PDS"
    sat_id=$(echo $data_file | cut -c1-4)
    
    # Get MODIS Level 0 data file if we don't have it already and create event file
    if [ ! -e $target_dir/$data_file ]; then

      echo "Ingesting "$data_file" from "$source_terra_dir" at "$(date)

      if [ "$sat_id" == "P042" ]; then

        # Get Terra MODIS Level 0 file
        lftp -c "get -c $source_terra_dir/$data_file -o $target_dir/$data_file"

      else
      
        # Get Aqua MODIS Level 0 file
        lftp -c "get -c $source_aqua_dir/$data_file -o $target_dir/$data_file"

        # Get Aqua GBAD Level 0 data file
        gbad_file="P1540957"$(echo $data_file | cut -c9-)
        lftp -c "get -c $source_gbad_dir/$gbad_file -o $target_dir/$gbad_file"

        # Get AIRS, AMSU, AMSR-E, HSB Level 0 data files
        apid_list="261 262 290 342 402 404 405 406 407 414 415 957"
        for apid in $apid_list; do
          airs_file=$(echo $data_file | sed 's/P1540064/P1540'$apid'/')
          lftp -c "get -c $source_airs_dir/$airs_file -o $target_dir/$airs_file"
        done

      fi

      # Create the event file
      touch $target_dir/$data_file.event

    fi

  done

  # Wait before checking again
  sleep $DBVM_FTP_INTERVAL

done

exit 0
