#!/bin/bash

# Stop the DBVM system

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

echo "(Stopping the DBVM system)"

echo "(Removing crontab)"
crontab -l &> $DBVM_HOME/crontab.backup
crontab -r &> /dev/null

echo "(Killing daemons)"
killall -q -u $LOGNAME -e daemon_pds_event.bash
killall -q -u $LOGNAME -e daemon_get_ftp_data.bash
killall -q -u $LOGNAME -e daemon_get_local_data.bash

exit 0
