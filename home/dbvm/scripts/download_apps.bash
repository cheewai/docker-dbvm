#!/bin/bash

# Download DBVM software packages

#------------------------------------------------------------------------------
# Download a file to the current directory (given a ftp or http URL)
function download_file {
source=$(dirname $1)
file=$(basename $1)
echo lftp -c "mirror -I $file $source ."
lftp -c "mirror -i $file $source ."
if [ $? -ne 0 ]; then 
  echo Error downloading $url/$file
  exit 1
fi
}
#------------------------------------------------------------------------------

echo
echo "(Downloading DBVM processing packages)"

# Switch to DBVM tarfile directory
cd $DBVM_HOME/tarfiles

# Download MODIS Level 1 processing packages
if [ "$DBVM_LEVEL1" == "YES" ]; then
  download_file http://www.sat.dundee.ac.uk/~arb/eoslzx-free/eoslzx-free-223.zip
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/imapp_va/packages/GBAD2.6_SPA.tar.gz
  download_file ftp://samoa.gsfc.nasa.gov/seadas/modisl1db/modisl1db_1.7/modisl1db_linux.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/ds/DESTRIPE.tar.gz
fi

# Download DEM data for MODISL1DB
if [ "$DBVM_DEM" == "YES" ]; then
  download_file ftp://samoa.gsfc.nasa.gov/seadas/modisl1db/modisl1db_1.7/seadas_dem_modis.tar.gz
fi

# Download MODIS Level 2 Atmosphere processing packages
if [ "$DBVM_ATMOS" == "YES" ]; then
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/modisl2_v2.1/IMAPP_MODISL2_V2.1.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/Level-2/v5524/vis/McLITE-linux-imapp-1.5.tar.gz
fi

# Download MODIS Image processing packages
if [ "$DBVM_IMAGE" == "YES" ]; then
  download_file ftp://loa630.univ-lille1.fr/HDFLOOK/LINUX_INTEL32_HDFLook.tar.gz
  download_file ftp://loa630.univ-lille1.fr/HDFLOOK/MAPS.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/dbge/dbge_v1.2/dbge_v1.2.tar.gz
fi

# Download MODIS Level 2 Land processing packages
if [ "$DBVM_LAND" == "YES" ]; then
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/imapp_va/packages/MOD14_5.0.1_SPA.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/imapp_va/packages/NDVIEVI2.2_SPA.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/imapp_va/packages/MODLST_4.14_SPA.tar.g
fi

# Download MODIS Level 2 Ocean processing packages
if [ "$DBVM_OCEAN" == "YES" ]; then
  download_file ftp://samoa.gsfc.nasa.gov/seadas/seadas_6.2/seadas_linux.tar.gz
  download_file ftp://samoa.gsfc.nasa.gov/seadas/seadas_6.2/seadas_processing_common.tar.gz
  download_file ftp://samoa.gsfc.nasa.gov/seadas/seadas_6.2/seadas_processing_linux.tar.gz
  download_file ftp://samoa.gsfc.nasa.gov/seadas/seadas_6.2/seadas_modisa.tar.gz
  download_file ftp://samoa.gsfc.nasa.gov/seadas/seadas_6.2/seadas_modist.tar.gz
fi

# Download MOD09 package
if [ "$DBVM_MOD09" == "YES" ]; then
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/imapp_va/packages/MOD09_5.3.18_SPA.tar.gz
fi

# Download AIRS packages
if [ "$DBVM_AIRS" == "YES" ]; then
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AIRS/v5524/AIRS_linux_v5.2.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AIRS/v5524/AIRS_store_v5.2.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AIRS/v5524/DEM30ARC.tar
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AIRS/v5524/STD30ARC.tar
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AIRS/v5524/AIRS_anc_avhrr.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AIRS/v5524/AIRS_anc_myd11.tar.gz
fi

# Download AMSR-E package
if [ "$DBVM_AMSRE" == "YES" ]; then
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AMSRE/IMAPP_UWAMSRE.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AMSRE/Level-2/v1.0/AMSRE_RainRate.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AMSRE/Level-2/v1.1/AMSRE_SM.tar.gz
  download_file ftp://ftp.ssec.wisc.edu/pub/IMAPP/AMSRE/Level-2/v1.2/AMSRE_SWE.tar.gz
fi

# Verify download packages
echo
echo "(Verifying downloaded packages)"
zip -T *.zip
if [ $? -ne 0 ]; then
  echo "Corrupt package detected. Please remove the corrupted file and run the DBVM installer again."
  exit 1
fi
gzip -tv *.gz
if [ $? -ne 0 ]; then
  echo "Corrupt package detected. Please remove the corrupted file and run the DBVM installer again."
  exit 1
fi

exit 0
