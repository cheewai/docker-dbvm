#!/bin/bash

# Main driver script for DBVM processing
# Usage: db_main.bash pds_file

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: db_main.bash pds_file"
  echo "where"
  echo "pds_file is the full path and name of the input Level 0 PDS file"
  exit 1
fi

# Get input PDS file name
pds_file=$1
if [ ! -e $pds_file ]; then
  echo "Level 0 PDS file does not exist: "$pds_file
  exit 1
fi

# Set up logfiles
LOG=$LOG_DIR/$(basename $pds_file).db_main.log
ERR=$LOG_DIR/$(basename $pds_file).db_main.err
exec 1>$LOG 2>$ERR

# Save start directory
start_dir=$PWD

# Make data directories (just in case they were deleted)
make_dirs.bash &> /dev/null
 
#--------------------------------------------------------------------------------
# MODIS Level 1 Processing
#--------------------------------------------------------------------------------
echo
echo "========================================================================="
echo "Starting MODIS Level 1 processing at "$(date -u)
echo db_modis_level1.bash $pds_file
db_modis_level1.bash $pds_file
if [ $? -ne 0 ]; then
  echo "Level 1 processing failed for input file: "$pds_file
  cd $start_dir
  mv $LOG $ERROR_DIR
  mv $ERR $ERROR_DIR
  exit 1
fi

# Get name of L1B 1KM HDF file from text file created by db_modis_level1.bash
text_file=$(basename $pds_file).modis.level1
file_1km=$L1_DIR/$(cat $L1_DIR/$text_file)
rm $L1_DIR/$text_file

# Get satellite/date/time (e.g., a1.06260.2057)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Make log directory for this pass (e.g., $LOG_DIR/a1.06260.2057)
mkdir $LOG_DIR/$date_time

# Store logfiles created by db_modis_level1.bash
mv $LOG_DIR/$(basename $pds_file).modis.level1.log $LOG_DIR/$date_time/$date_time.modis.level1.log
mv $LOG_DIR/$(basename $pds_file).modis.level1.err $LOG_DIR/$date_time/$date_time.modis.level1.err

#--------------------------------------------------------------------------------
# MODIS Level 2 Atmosphere Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_ATMOS" == "YES" ]; then
  echo "Starting MODIS Level 2 Atmosphere processing at "$(date -u)
  echo db_modis_atmos.bash $file_1km &
  db_modis_atmos.bash $file_1km &
fi

#--------------------------------------------------------------------------------
# MODIS Level 2 Land Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_LAND" == "YES" ]; then
  echo "Starting MODIS Level 2 Land processing at "$(date -u)
  echo db_modis_land.bash $file_1km &
  db_modis_land.bash $file_1km &
fi 

#--------------------------------------------------------------------------------
# MODIS Level 2 Ocean Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_OCEAN" == "YES" ]; then
  echo "Starting MODIS Level 2 Ocean processing at "$(date -u)
  echo db_modis_ocean.bash $file_1km &
  db_modis_ocean.bash $file_1km &
fi

#--------------------------------------------------------------------------------
# MODIS Level 2 Google Earth Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_DBGE" == "YES" ]; then
  echo "Starting MODIS Level 2 Google Earth processing at "$(date -u)
  echo db_modis_dbge.bash $file_1km &
  db_modis_dbge.bash $file_1km &
fi

#--------------------------------------------------------------------------------
# AIRS Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_AIRS" == "YES" ]; then
  sat_id=$(basename $pds_file | cut -c1-8)
  if [ "$sat_id" == "P1540064" ]; then
    echo "Starting AIRS processing at "$(date -u)
    airs_pds_file=$(echo $pds_file | sed 's/P1540064/P1540404/')
    echo db_airs.bash $airs_pds_file $file_1km &
    db_airs.bash $airs_pds_file $file_1km &
  fi
fi

#--------------------------------------------------------------------------------
# AMSR-E Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_AMSRE" == "YES" ]; then
  sat_id=$(basename $pds_file | cut -c1-8)
  if [ "$sat_id" == "P1540064" ]; then
    echo "Starting AMSR-E processing at "$(date -u)
    amsr_pds_file=$(echo $pds_file | sed 's/P1540064/P1540402/')
    echo db_amsre.bash $amsr_pds_file $file_1km &
    db_amsre.bash $amsr_pds_file $file_1km &
  fi
fi

#--------------------------------------------------------------------------------
# User Defined Processing
#--------------------------------------------------------------------------------
if [ "$DBVM_USER" == "YES" ]; then
  echo "Starting User Defined processing at "$(date -u)
  echo db_user.bash $file_1km &
  db_user.bash $file_1km &
fi

#--------------------------------------------------------------------------------
# Store logfiles created by db_main.bash
#--------------------------------------------------------------------------------
mv $LOG $LOG_DIR/$date_time/$date_time.db_main.log
mv $ERR $LOG_DIR/$date_time/$date_time.db_main.err

exit 0
