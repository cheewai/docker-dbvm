#!/bin/bash

# Script for MODIS image processing (HDFLook)
# Liam.Gumley@ssec.wisc.edu
# 29-APR-2009

echo
echo "Creating MODIS image products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------
# Check input arguments
if [ $# -ne 3 ]; then
  echo "Usage: run_modis_atmos.bash input_file template_file output_file"
  echo "where"
  echo "input_file is the full path and name of the input MODIS product HDF file"
  echo "template_file is the full path and name of the HDFLook template file"
  echo "output_file is the full path and name of the output GeoTIFF image file"
  exit 1
fi

# Get arguments
input_file=$1
template_file=$2
output_file=$3

# Check for input files
if [ ! -r $input_file ]; then
  echo "Input file not found: "$input_file
  exit 1
fi
if [ ! -r $template_file ]; then
  echo "Template file not found: "$template_file
  exit 1
fi

#-------------------------------------------------------------------------------
# Run HDFLook image processing
#-------------------------------------------------------------------------------
# Create HDFLook script
cat $template_file | \
  sed "s/INPUT_FILE/$input_file/g" | \
  sed "s/OUTPUT_FILE/$output_file/g" > $input_file.scr

# Set up HDFLook
export HDFLOOKTMP=.
export HDFLOOKMAPS=$DBVM_HOME/apps/hdflook/MAPS

# Copy palettes to this directory
cp $DBVM_HOME/scripts/palette_*.txt .

# Run HDFLook
echo $DBVM_HOME/apps/hdflook/LINUX_INTEL32/HDFLook $input_file.scr
$DBVM_HOME/apps/hdflook/LINUX_INTEL32/HDFLook $input_file.scr
if [ $? -ne 0 ]; then
  echo "Error in HDFLook processing for file: "$input_file
  exit 1
fi

# Convert GeoTIFF to JPEG
echo convert -quiet -quality 80 $output_file ${output_file%.tif}.jpg
convert -quiet -quality 80 $output_file ${output_file%.tif}.jpg

# Clean up
rm palette_*.txt $input_file.scr

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished MODIS image products at "$(date -u)
exit 0
