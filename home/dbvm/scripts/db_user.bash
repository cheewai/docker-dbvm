#!/bin/bash

# Driver script for MODIS DB User Defined processing (calls $DBVM_HOME/user/run_user.bash)
# Liam.Gumley@ssec.wisc.edu

# Check for user script
script=$DBVM_HOME/user/run_user.bash
if [ ! -e $script ]; then
  echo "User defined processing script was not found: "$script
  exit 1
fi

# Get input L1B 1KM file name
file_1km=$1

# Get satellite/date/time (e.g., a1.06260.2057)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Switch to work directory
start_dir=$PWD
temp_dir=$WORK_DIR/$date_time.user
mkdir $temp_dir &>/dev/null
cd $temp_dir
if [ $? -ne 0 ]; then
  echo "Could not switch to work directory: "$temp_dir
  exit 1
fi

# Set up logfile and errfile
LOG=$LOG_DIR/$date_time/$date_time.user.log
ERR=$LOG_DIR/$date_time/$date_time.user.err
exec 1>$LOG 2>$ERR

#--------------------------------------------------------------------------------
# User Defined Processing
#--------------------------------------------------------------------------------

# Run User Defined Processing
echo $DBVM_HOME/user/run_user.bash $file_1km
$DBVM_HOME/user/run_user.bash $file_1km
if [ $? -ne 0 ]; then
  echo "User processing failed for input file: "$file_1km
  cd $start_dir
  mv $temp_dir $ERROR_DIR
  mv $LOG $ERROR_DIR
  mv $ERR $ERROR_DIR
  exit 1
fi

# Copy products to archive (.hdf, .jpg, .tif)
if [ "$DBVM_ARCHIVE" == "YES" ]; then

  echo
  echo "(Copying products to archive in "$DBVM_ARCHIVE_DIR")"
  echo

  # Get archive directory
  if [ $(basename $file_1km | cut -c1-2) == "t1" ]; then sat_name="terra"; else sat_name="aqua"; fi
  date_string=$(passid_to_date.bash $file_1km)

  # Copy the product files to archive
  rsync -av *.hdf $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level2
  rsync -av *.jpg $DBVM_ARCHIVE_DIR/$sat_name/$date_string/images
  rsync -av *.tif $DBVM_ARCHIVE_DIR/$sat_name/$date_string/images

fi

# Move product files
mv *.hdf $L2_DIR
mv *.jpg $IMAGE_DIR
mv *.tif $IMAGE_DIR

# Clean up
cd $start_dir
rm -rf $temp_dir

# Exit
exit 0

