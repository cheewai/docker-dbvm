#!/bin/bash

# Script for AIRS Level 1 and Level 2 processing
# Liam.Gumley@ssec.wisc.edu

echo
echo "Creating AIRS Level 1 and Level 2 products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------

# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_airs.bash pds_file"
  echo "where"
  echo "pds_file is the full path and name of the input AIRS APID 404 Level 0 PDS file"
  exit 1
fi

# Get input PDS file name and pass id
pds_file=$1
pass_id=$(basename $pds_file)
echo "Input AIRS Level 0 file: "$pds_file

# Check for input PDS file
if [ ! -r $pds_file ]; then
  echo "Input AIRS Level 0 file not found: "$pds_file
  exit 1
fi

# Link the required PDS files to the current directory
apid_list="261 262 290 342 404 405 406 407 414 415 957"
for apid in $apid_list; do
  file=$(echo $pds_file | sed 's/P1540404/P1540'$apid'/')
  if [ ! -r $file ]; then
    echo "Required input file was not found: "$file
    exit 1
  fi
  ln -f -s $file $PWD
done

#-------------------------------------------------------------------------------
# Run AIRS Level 1 and Level 2 processing
#-------------------------------------------------------------------------------
echo
echo "(Starting AIRS Level 1 and Level 2 processing)"

# Set AIRS DB environment variables
export AIRSROOT=$DBVM_HOME/apps/airs
export AIRSDB=$AIRSROOT
export AIRSDB_HOME=$AIRSROOT
export ARCH=linux
export ARCH_TOOLKIT=linux
export PATH=$AIRSROOT/$ARCH/bin:$AIRSDB/script:$PATH
export DEM_DATA=$AIRSROOT/DEM
export PGSHOME=$AIRSROOT/toolkit
export HDFHOME=$AIRSROOT/hdf
export HDFEOS_HOME=/dev/null

# Set location of runtime files
export AIRSDB_PACKETS=$PWD
export GBAD_DATA=$PWD
export RUN_DIR=$PWD

# Link the NORAD TLE file to the current directory
ln -f -s $DBVM_HOME/scripts/noradfile $PWD

# Run the AIRS processing script
echo AirsDb.py -l 2 -s
AirsDb.py -l 2 -s
if [ $? -ne 0 ]; then
  echo "AIRS Level 1 and Level 2 processing failed for input file: "$pds_file
  exit 1
fi

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished AIRS products at "$(date -u)

exit 0
