#!/bin/bash

# Script for MODIS Level 2 ocean processing (SEADAS)
# Liam.Gumley@ssec.wisc.edu
# 06-APR-2009

echo
echo "Creating MODIS Level 2 ocean products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------
# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_modis_ocean.bash file_1km"
  echo "where"
  echo "file_1km is the full path and name of the input MODIS Level 1B 1KM HDF file"
  exit 1
fi

# Get input L1B 1KM file name and directory
file_1km=$1
dir_1km=$(dirname $file_1km)
echo "Input Level 1B 1KM file: "$file_1km

# Check for input L1B 1KM file
if [ ! -r $file_1km ]; then
  echo "Input L1B 1KM file not found: "$file_1km
  exit 1
fi

# Get satellite/date/time (e.g., a1.06260.2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Get pass year, day of year, time (e.g, 06 260 2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
year=$(basename $file_1km | cut -c4-5)
jday=$(basename $file_1km | cut -c6-8)
time=$(basename $file_1km | cut -c10-13)

# Get GEO file name
file_geo=$dir_1km/$date_time.geo.hdf

#-------------------------------------------------------------------------------
# Get ancillary data
#-------------------------------------------------------------------------------
echo
echo "(Downloading SEADAS ancillary data)"

# Set up SeaDAS
export SEADAS=$DBVM_HOME/apps/seadas
source $DBVM_HOME/scripts/env_seadas.bash

# Get ancillary data
start_time="20"$(echo $date_time | cut -c4-8,10-13)"00"
echo getanc -start $start_time -no-no2
getanc -start $start_time -no-no2
anc_status=$?
anc_file=$start_time.anc

#-------------------------------------------------------------------------------
# Run SEADAS ocean product processing
#-------------------------------------------------------------------------------
echo
echo "(Starting SEADAS ocean product processing)"

# Run SeaDAS Level 2 ocean processing
ulimit -Ss unlimited
file_out=$date_time.seadas.hdf
if [ $anc_status -le 30 ]; then
  echo l2gen ifile=$file_1km geofile=$file_geo ofile=$file_out l2prod="default sst sst4" proc_land=1 par=$anc_file
  l2gen ifile=$file_1km geofile=$file_geo ofile=$file_out l2prod="default sst sst4" proc_land=1 par=$anc_file
else
  echo l2gen ifile=$file_1km geofile=$file_geo ofile=$file_out l2prod="default sst sst4" proc_land=1
  l2gen ifile=$file_1km geofile=$file_geo ofile=$file_out l2prod="default sst sst4" proc_land=1
fi
if [ $? -ne 0 ]; then
  echo "SEADAS processing failed for input file: "$file_1km
  exit 1
fi
echo "(Finishing SEADAS ocean product processing)"

#-------------------------------------------------------------------------------
# Run MODIS image processing
#-------------------------------------------------------------------------------
if [ "$DBVM_IMAGE" == "YES" ]; then
echo
echo "(Starting MODIS image processing)"
modocn_file=MODOCN.A20$year$jday.$time.hdf
mod03_file=MOD03.A20$year$jday.$time.hdf
ln -f -s $file_out $modocn_file
ln -f -s $file_geo $mod03_file
$DBVM_HOME/scripts/run_modis_hdflook.bash $modocn_file $DBVM_HOME/scripts/template_sst.txt $date_time.sst.tif
nscans=$(ncdump -h $file_1km | grep Day | grep mode |grep scans | sed 's/_//g' | awk '{print $7}')
if [ $nscans -ge 5 ]; then
  $DBVM_HOME/scripts/run_modis_hdflook.bash $modocn_file $DBVM_HOME/scripts/template_chla.txt $date_time.chla.tif
fi
rm $modocn_file $mod03_file
echo "(Finishing MODIS image processing)"
fi

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished MODIS Level 2 ocean products at "$(date -u)

# Clean up
rm $anc_file &> /dev/null

exit 0
