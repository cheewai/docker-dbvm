#!/bin/bash

# Install MODIS Ocean processing applications

echo
echo "Starting installation of MODIS Ocean processing applications, please wait..."

echo "(Installing SEADAS)"
cd $DBVM_HOME/apps
mkdir seadas
cd seadas
tar zxf $DBVM_HOME/tarfiles/seadas_linux.tar.gz
tar zxf $DBVM_HOME/tarfiles/seadas_processing_common.tar.gz
tar zxf $DBVM_HOME/tarfiles/seadas_processing_linux.tar.gz
tar zxf $DBVM_HOME/tarfiles/seadas_modisa.tar.gz
tar zxf $DBVM_HOME/tarfiles/seadas_modist.tar.gz

echo "Finished installation of MODIS Ocean processing applications"
exit 0

