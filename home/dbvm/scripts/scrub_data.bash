#!/bin/bash

# Scrub DBVM data and error directories to remove old files.
#
# NOTE: This script is intended to run via crontab,
# so the DBVM setup script must be run first (see below).

# Run the DBVM setup script (DBVM_HOME is set in the crontab file)
source $DBVM_HOME/scripts/dbvm_env.bash

# Send output to logfile
LOG=$LOG_DIR/scrub_data.log
exec >>$LOG
exec 2>>$LOG
echo "________________________"
echo $0 started at `date -u`

# Scrub data files (note trailing slash in path!)
# All files older than DBVM_RETAIN days will be deleted
find $DATA_DIR/ -type f -mtime +$DBVM_RETAIN -exec /bin/rm {} \;
if [ $? -ne 0 ]; then
  echo "Error detected during $DATA_DIR file scrub"
  exit 1
fi

# Scrub error files (note trailing slash in path!)
# All files older than 14 days and larger than 10 MB will be deleted
find $ERROR_DIR/ -type f -mtime +14 -size +10M -exec /bin/rm {} \;
if [ $? -ne 0 ]; then
  echo "Error detected during $ERROR_DIR file scrub"
  exit 1
fi

exit 0
