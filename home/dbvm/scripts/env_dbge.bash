# Path for DBGE scripts and binaries
export PATH=$DBGE_HOME/scripts:$DBGE_HOME/bin:$PATH

# FL setup
export FL_DIR=$DBGE_HOME/fl/fl_0.79a
export PATH=$FL_DIR/bin:$PATH

# Define IDL command
export IDL_CMD="fl -32 --ncpu=1"

# IDL startup file
export IDL_STARTUP=$DBGE_HOME/idl/idl_startup.pro

# DBGE tile and kml directories
export DBGE_TILE_DIR=$DATA_DIR/tiles
export DBGE_KML_DIR=$DATA_DIR/kml

# DBGE URL for KML files
export DBGE_KML_URL="http://sunset.ssec.wisc.edu/kml/"

# Local DB site name, lat, lon
export DBGE_SITE_NAME="SSEC"    # Note: all capital letters, no spaces allowed
export DBGE_SITE_LAT="43.071"
export DBGE_SITE_LON="-89.406"
