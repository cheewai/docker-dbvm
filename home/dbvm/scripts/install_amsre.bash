#!/bin/bash

# Install AMSR-E processing applications

echo
echo "Starting installation of AMSR-E processing applications, please wait..."

echo "(Installing AMSR-E)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/IMAPP_UWAMSRE.tar.gz

echo "(Installing AMSR-E patches)"
rsync -aC $DBVM_HOME/scripts/patches/imapp_uwamsre $DBVM_HOME/apps

echo "Finished installation of AMSR-E processing applications"
exit 0
