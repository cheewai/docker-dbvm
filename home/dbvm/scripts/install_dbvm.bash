#!/bin/bash

# Install the DBVM package

# Check DBVM_HOME environment variable
if [ -z "$DBVM_HOME" ]; then
  echo 'ERROR: Environment variable $DBVM_HOME is not set'
  exit 1
fi
if [ ! -d "$DBVM_HOME" ]; then
  echo "ERROR: DBVM_HOME directory "$DBVM_HOME"does not exist"
  exit 1
fi

# Run the DBVM setup script
source $DBVM_HOME/scripts/dbvm_env.bash

echo
echo "INSTALLING DIRECT BROADCAST VIRTUAL MACHINE (DBVM)"

# Check with user before proceeding
echo
echo "You are about to install DBVM in "$DBVM_HOME
echo "Previous versions of DBVM applications will be removed. Data directories will be preserved".
echo
read -p "Do you wish to proceed? [yes or no]: " answer
if [ "$answer" != "yes" ]; then
  echo "DBVM installation aborted"
  exit 1
fi

# Switch to DBVM directory
cd $DBVM_HOME
if [ $? -ne 0 ]; then
  echo "ERROR: Could not switch to DBVM home directory "$DBVM_HOME
  exit 1
fi

# Stop DBVM if it is runnning
echo
dbvm_stop.bash

# Make DBVM directories
make_dirs.bash &> /dev/null

# Download DBVM processing packages
echo
read -p "Do you wish to download processing packages? [yes or no]: " answer
if [ "$answer" == "yes" ]; then download_apps.bash; fi

# Remove contents of applications directory
rm -rf $DBVM_HOME/apps/*

# Install DBVM processing packages
if [ "$DBVM_LEVEL1" == "YES" ]; then install_level1.bash; fi
if [ "$DBVM_IMAGE"  == "YES" ]; then install_image.bash; fi
if [ "$DBVM_ATMOS"  == "YES" ]; then install_atmos.bash; fi
if [ "$DBVM_LAND"   == "YES" ]; then install_land.bash; fi
if [ "$DBVM_OCEAN"  == "YES" ]; then install_ocean.bash; fi
if [ "$DBVM_AIRS"   == "YES" ]; then install_airs.bash; fi
if [ "$DBVM_AMSRE"  == "YES" ]; then install_amsre.bash; fi

# Create local version of crontab.txt
echo
echo "(Creating local version of crontab.txt)"
sed 's:DBVM_INSTALL_DIR:'$DBVM_HOME':' $DBVM_HOME/scripts/crontab.template > $DBVM_HOME/scripts/crontab.txt

# Start DBVM
echo
dbvm_start.bash

echo
echo "DBVM INSTALLATION COMPLETE"
echo
echo "To start processing data, copy a MODIS Level 0 PDS file to"
echo $DBVM_HOME"/data/level0"
echo "and create an event file, e.g."
echo "cp P0420064AAAAAAAAAAAAAA09310163001001.PDS "$DBVM_HOME"/data/level0"
echo "touch P0420064AAAAAAAAAAAAAA09310163001001.PDS.event"
echo
echo "Sample MODIS Level 0 PDS files can be obtained from"
echo "ftp://ftp.ssec.wisc.edu/pub/eosdb/pds/"
echo

exit 0
