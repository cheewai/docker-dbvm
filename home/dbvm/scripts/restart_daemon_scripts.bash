#!/bin/bash 

# Restart all DBVM daemon processing scripts.
#
# This script must be run in the any of the following circumstances:
# 1. When DBVM is installed (install_dbvm.bash)
# 2. When the DBVM environment configuration file is modified (dbvm_env.bash)
# 3. When a DBVM daemon script is modified (daemon*.bash)

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

# Restart Level 0 event script (should be running always)
script=daemon_pds_event.bash
killall -q -u $LOGNAME -e $script
sleep 3
echo "Starting "$script
($script &)

# Restart local data ingest script (run if DBVM_LOCAL_DATA="YES")
script=daemon_get_local_data.bash
killall -q -u $LOGNAME -e $script
sleep 3
if [ "$DBVM_LOCAL_DATA" == "YES" ]; then
  echo "Starting "$script
  ($script &)
fi

# Restart FTP data ingest script (run if DBVM_FTP_DATA="YES")
script=daemon_get_ftp_data.bash
killall -q -u $LOGNAME -e $script
sleep 3
if [ "$DBVM_FTP_DATA" == "YES" ]; then
  echo "Starting "$script
  ($script &)
fi

exit 0
