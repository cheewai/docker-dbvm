#!/bin/bash

# Script for AMSR-E Level 1 and Level 2 processing
# Liam.Gumley@ssec.wisc.edu

echo
echo "Creating AMSR-E Level 1 and Level 2 products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------

# Check input arguments
if [ $# -ne 2 ]; then
  echo "Usage: run_amsre.bash pds_file date_time"
  echo "where"
  echo "pds_file is the AMSR-E APID 402 Level 0 PDS file (e.g., P1540402AAAAAAAAAAAAAA10053183612001.PDS)"
  echo "date_time is the satellite/date/time (e.g., a1.06260.2057)"
  exit 1
fi

# Get arguments
pds_file=$1
date_time=$2
echo "AMSR-E PDS file: "$pds_file
echo "DATE/TIME: "$date_time

# Check for input files
if [ ! -r $pds_file ]; then
  echo "Input AMSR-E Level 0 file not found: "$pds_file
  exit 1
fi

# Get GBAD file name
gbad_file=$(echo $pds_file | sed 's/P1540402/P1540957/')

# Link the required PDS files to the current directory
ln -f -s $pds_file $date_time.402.pds
ln -f -s $gbad_file $date_time.957.pds

#-------------------------------------------------------------------------------
# Run AMSR-E Level 1  processing
#-------------------------------------------------------------------------------
echo
echo "(Starting AMSR-E Level 1 processing)"

# Set environment variables
export AMSRE_ROOT=$DBVM_HOME/apps/imapp_uwamsre
source $AMSRE_ROOT/env/imapp_uwamsre_l2.bash_env 

# Run the AMSR-E processing script
echo amsre_level1b.csh $date_time
amsre_level1b.csh $date_time
if [ $? -ne 0 ]; then
  echo "AMSR-E Level 0 to Level 1B processing failed for input file: "$pds_file
  exit 1
fi

# Remove PDS files
rm -f $date_time.402.pds $date_time.957.pds

# Set Level 1 output file names
sci_file=$date_time"_science_data.dat"
geo_file=$date_time"_scantime_pos_vel_geoloc.dat"

#-------------------------------------------------------------------------------
# Run AMSR-E Level 2  processing
#-------------------------------------------------------------------------------
echo
echo "(Starting AMSR-E Level 2 processing)"

# Rain rate
echo run_amsre_rainrate.csh $sci_file $geo_file $PWD
run_amsre_rainrate.csh $sci_file $geo_file $PWD
if [ $? -ne 0 ]; then
  echo "AMSR-E Rain Rate processing failed for input file: "$sci_file
  exit 1
fi

# Soil moisture
export LD_LIBRARY_PATH=$AMSRE_ROOT/bin
echo run_amsre_sm.csh $sci_file $geo_file $PWD
run_amsre_sm.csh $sci_file $geo_file $PWD
if [ $? -ne 0 ]; then
  echo "AMSR-E Soil Moisture processing failed for input file: "$sci_file
  exit 1
fi

# Snow water equivalent
echo run_amsre_swe.csh $sci_file $geo_file $PWD
run_amsre_swe.csh $sci_file $geo_file $PWD
if [ $? -ne 0 ]; then
  echo "AMSR-E Snow Water Equivalent processing failed for input file: "$sci_file
  exit 1
fi

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished AMSR-E products at "$(date -u)

exit 0
