# Note this file must be executed via "$ source env_imapp.bash"
export LOCAL_ANC_DIR=${DBVM_HOME}/data/ancillary/imapp_modisl2
export REMOTE_ANC_DIR=ftp://ftp.ssec.wisc.edu/pub/eosdb/ancillary
export HDF_HOME=${MODIS_L2_HOME}/hdf
export MODIS_L2_CFG=${MODIS_L2_HOME}/config
export MODIS_L2_COEFF=${MODIS_L2_HOME}/coeff
export MODIS_L2_BIN=${MODIS_L2_HOME}/bin
export MODIS_L2_IMAGES=$PWD
export MODIS_L2_XSIZE=900
export MODIS_L2_YSIZE=900
export OUTPUT_TYPE=2
export PATH=.:${MODIS_L2_BIN}:${MODIS_L2_HOME}/scripts:${PATH}
