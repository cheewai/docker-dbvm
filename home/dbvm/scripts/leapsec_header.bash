#!/bin/bash
# Print a valid leapsec.dat header line for the current time
date1="$(date -u +'%Y-%m-%dT%H:%M:%SZ')"
date2="$(date -u +'%b  %d  %H:%M' -d '1 day ago')"
echo "Checked(unchanged): $date1 using USNO tai-utc.dat file of $date2"
exit 0
