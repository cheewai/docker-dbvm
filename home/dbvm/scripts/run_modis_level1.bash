#!/bin/bash

# Script for MODIS Level 0 to Level 1B processing
# Liam.Gumley@ssec.wisc.edu
# Note: This version is for MODISL1DB v1.7 or higher

echo
echo "Creating MODIS Level 1B products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------

# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_modis_level1.bash pds_file"
  echo "where"
  echo "pds_file is the full path and name of the input MODIS APID 64 Level 0 PDS file"
  exit 1
fi

# Get input PDS file name and pass id
pds_file=$1
pass_id=$(basename $pds_file)
echo "Input Level 0 file: "$pds_file

# Check for input PDS file
if [ ! -r $pds_file ]; then
  echo "Input Level 0 file not found: "$pds_file
  exit 1
fi

# Get satellite ID (P0420064 = Terra, P1540064 = Aqua)
sat_id=$(basename $pds_file | cut -c1-8)

#-------------------------------------------------------------------------------
# Run EOSLZX quicklook processing
#-------------------------------------------------------------------------------
echo
echo "(Starting EOSLZX quicklook processing)"

# Set up EOSLZX
export HSYSDIR=$DBVM_HOME/apps/eoslzx/sysconf
export HLOGDIR=.

# Set quicklook image file names
if [ "$sat_id" == "P0420064" ]; then eos_name="Terra"; else eos_name="Aqua"; fi
vis_file=$pass_id".ch17.jpg"
ir_file=$pass_id".ch31.jpg"
ln -f -s $pds_file ./eoslzx-tmp.pds

# Create MODIS visible (band 17) quicklook
echo $DBVM_HOME/apps/eoslzx/linux/eoslzx-free -S $eos_name -c 17 -f ./eoslzx-tmp.pds
$DBVM_HOME/apps/eoslzx/linux/eoslzx-free -S $eos_name -c 17 -f ./eoslzx-tmp.pds
if [ $? -ne 0 ]; then
  echo "EOSLZX processing for band 17 failed for input file: "$pds_file
fi
echo convert -resize 512 -normalize -quality 95 eoslzx-tmp.pds_ch17.gif $vis_file
convert -resize 512 -normalize -quality 95 eoslzx-tmp.pds_ch17.gif $vis_file

# Create MODIS infrared (band 31) quicklook
echo $DBVM_HOME/apps/eoslzx/linux/eoslzx-free -S $eos_name -c 31 -f ./eoslzx-tmp.pds
$DBVM_HOME/apps/eoslzx/linux/eoslzx-free -S $eos_name -c 31 -f ./eoslzx-tmp.pds
if [ $? -ne 0 ]; then
  echo "EOSLZX processing for band 31 failed for input file: "$pds_file
fi
echo convert -resize 512 -normalize -negate -quality 95 eoslzx-tmp.pds_ch31.gif $ir_file
convert -resize 512 -normalize -negate -quality 95 eoslzx-tmp.pds_ch31.gif $ir_file
echo "(Finishing EOSLZX quicklook processing)"

#-------------------------------------------------------------------------------
# Run GBAD processing (for Aqua only)
#-------------------------------------------------------------------------------
sat_id=$(basename $pds_file | cut -c1-8)
if [ "$sat_id" == "P1540064" ]; then

  echo
  echo "(Starting GBAD processing)"

  # Check for Aqua GBAD file
  gbad_file=$(dirname $pds_file)"/P1540957"$(basename $pds_file | cut -c9-)
  if [ ! -r $gbad_file ]; then
    echo "Aqua GBAD Level 0 file not found: "$gbad_file
    exit 1
  fi
  
  # Run GBAD processor
  touch configfile
  echo $DBVM_HOME/apps/gbad/aqua_main -packetfile $gbad_file \
    -noradfile $DBVM_HOME/apps/gbad/localdata/noradfile \
    -attitudefile aqua.att -ephemerisfile aqua.eph -listconfig yes
  $DBVM_HOME/apps/gbad/aqua_main -packetfile $gbad_file \
    -noradfile $DBVM_HOME/apps/gbad/localdata/noradfile \
    -attitudefile aqua.att -ephemerisfile aqua.eph -listconfig yes
  if [ $? -ne 0 ]; then
    echo "GBAD processing failed for input file: "$gbad_file
    exit 1
  fi
  rm configfile
  
fi

#-------------------------------------------------------------------------------
# Run MODISL1DB unpacking and geolocation processing
#-------------------------------------------------------------------------------
echo
echo "(Starting MODISL1DB unpacking and geolocation processing)"

# Set up MODISL1DB
export DBHOME=$DBVM_HOME/apps/modisl1db
source $DBHOME/run/scripts/modisl1db_env.bash
export OCSSW_DEBUG="0"

# Set DEM flag
dem_file=$DBHOME/run/data/modis/static/dem30ARC_E60N0.hdf
if [ ! -r $dem_file ]; then
  dem_flag="-disable-dem"
else
  dem_flag=""
fi

# Set up retry counter (to handle bad input PDS files)
skip_beg=(2 15 30 60 120 300  2 15 15  15  15)
skip_end=(2  2 15 15  15  15 15 30 60 120 300)
index=0
status=false

# Run MODISL1DB L0 to L1A processing
while [ "$status" == "false" ] && [ $index -le 10 ]; do

  if [ "$sat_id" == "P0420064" ]; then

    # Process Terra MODIS with real-time ephemeris
    echo modis_L1A.csh $pds_file -startnudge ${skip_beg[$index]} -stopnudge ${skip_end[$index]}
    modis_L1A.csh $pds_file -startnudge ${skip_beg[$index]} -stopnudge ${skip_end[$index]}
    file_l1a=$(ls -1tr *.L1A_LAC | tail -1)
    echo modis_GEO.csh $file_l1a -disable-definitive -disable-utcpole_leapsec -geocheck_threshold 80 $dem_flag
    modis_GEO.csh $file_l1a -disable-definitive -disable-utcpole_leapsec -geocheck_threshold 80 $dem_flag
      
  else

    # Process Aqua MODIS with real-time ephemeris
    echo modis_L1A.csh $pds_file -startnudge ${skip_beg[$index]} -stopnudge ${skip_end[$index]}
    modis_L1A.csh $pds_file -startnudge ${skip_beg[$index]} -stopnudge ${skip_end[$index]}
    file_l1a=$(ls -1tr *.L1A_LAC | tail -1)
    echo modis_GEO.csh $file_l1a -a1 aqua.att -e1 aqua.eph -disable-definitive -disable-utcpole_leapsec -geocheck_threshold 80 $dem_flag
    modis_GEO.csh $file_l1a -a1 aqua.att -e1 aqua.eph -disable-definitive -disable-utcpole_leapsec -geocheck_threshold 80 $dem_flag

  fi 

  # Check status
  if [ $? -ne 0 ]; then
    echo "Level 0 to Level 1A processing failed for input file: "$pds_file"; will retry"
  else
    status=true
  fi

  # Increment retry index
  if [ "$status" == "false" ]; then
    index=$(( $index + 1 ))
    echo
    echo "*** RETRY # "$index
    echo
  fi

done

# Check status after retries are done
if [ "$status" == "false" ]; then
  echo "Level 0 to Level 1A processing failed for input file: "$pds_file
  exit 1
fi
echo "(Finishing MODISL1DB unpacking and geolocation processing)"

#-------------------------------------------------------------------------------
# Run MODISL1DB calibration processing
#-------------------------------------------------------------------------------
echo
echo "(Starting MODISL1DB calibration processing)"
file_geo=$(ls -1tr *.GEO | tail -1)
echo modis_L1B.csh $file_l1a $file_geo
modis_L1B.csh $file_l1a $file_geo
if [ $? -ne 0 ]; then
  echo "Level 1A to Level 1B processing failed for input file: "$file_l1a
  exit 1
fi
echo "(Finishing MODISL1DB calibration processing)"

#-------------------------------------------------------------------------------
# RENAME OUTPUT FILES
#-------------------------------------------------------------------------------
# Get L1A, GEO, and L1B file names
file_l1a=$(ls -1tr *.L1A_LAC | tail -1)
file_id=$(basename $file_l1a | cut -c1-14)
file_geo=$file_id.GEO
file_1km=$file_id.L1B_LAC
file_hkm=$file_id.L1B_HKM
file_qkm=$file_id.L1B_QKM

# Get pass year, day of year, time (e.g, 09 120 0247) from L1A file name (e.g., T2009120024751.L1A_LAC)
year=$(echo $file_l1a | cut -c4-5)
jday=$(echo $file_l1a | cut -c6-8)
time=$(echo $file_l1a | cut -c9-12)

# Create pass id (e.g., t1.09030.0241)
if [ "$sat_id" == "P0420064" ]; then sat_name="t1"; else sat_name="a1"; fi
pass_id=$sat_name.$year$jday.$time

# Rename GEO, and L1B files
mv $file_geo $pass_id.geo.hdf
mv $file_1km $pass_id.1000m.hdf
if [ -r $file_hkm ]; then
  mv $file_hkm $pass_id.500m.hdf
  mv $file_qkm $pass_id.250m.hdf
fi

# If the granule has less than 5 day mode scans, then delete HKM and QKM files
nscans=$(ncdump -h $pass_id.1000m.hdf | grep Day | grep mode |grep scans | sed 's/_//g' | awk '{print $7}')
echo
echo "Number of day mode scans = "$nscans
if [ $nscans -lt 5 ] && [ -r $pass_id.500m.hdf ]; then
  echo "Granule has less than 5 day mode scans: HKM and QKM files will be deleted"
  rm $pass_id.500m.hdf $pass_id.250m.hdf
fi

#-------------------------------------------------------------------------------
# Create quicklook images
#-------------------------------------------------------------------------------

# Create quicklook images with titles
title=$eos_name' MODIS 20'$year$jday' '$time' UTC Band 17'
convert -fill gray16 -draw "rectangle 0,0 270,12" -fill white -pointsize 14 -draw "text 1,11 '$title'" \
  $vis_file $pass_id.ch17.jpg
title=$eos_name' MODIS 20'$year$jday' '$time' UTC Band 31'
convert -fill gray16 -draw "rectangle 0,0 270,12" -fill white -pointsize 14 -draw "text 1,11 '$title'" \
  $ir_file $pass_id.ch31.jpg

#-------------------------------------------------------------------------------
# Run MODIS L1B 1KM destriping
#-------------------------------------------------------------------------------
echo
echo "(Starting MODIS L1B 1KM destriping processing)"
export DESTRIPE_HOME=$DBVM_HOME/apps/destripe
source $DBVM_HOME/scripts/env_destripe.bash
if [ "$sat_id" == "P0420064" ]; then dest_name="terra"; else dest_name="aqua"; fi
echo $DBVM_HOME/apps/destripe/run_modis_destripe.csh $dest_name $pass_id.1000m.hdf
$DBVM_HOME/apps/destripe/run_modis_destripe.csh $dest_name $pass_id.1000m.hdf
if [ $? -ne 0 ]; then
  echo "Level 1B 1KM destriping processing failed for input file: "$pass_id.1000m.hdf
  exit 1
fi
echo "(Finishing MODIS L1B 1KM destriping processing)"

#-------------------------------------------------------------------------------
# Run MODIS image processing
#-------------------------------------------------------------------------------
if [ "$DBVM_IMAGE" == "YES" ]; then
echo
echo "(Starting MODIS image processing)"
mod021km_file=MOD021KM.A20$year$jday.$time.hdf
mod03_file=MOD03.A20$year$jday.$time.hdf
ln -f -s $pass_id.1000m.hdf $mod021km_file
ln -f -s $pass_id.geo.hdf $mod03_file
$DBVM_HOME/scripts/run_modis_hdflook.bash $mod021km_file $DBVM_HOME/scripts/template_band31.txt $pass_id.band31.tif
$DBVM_HOME/scripts/run_modis_hdflook.bash $mod021km_file $DBVM_HOME/scripts/template_band27.txt $pass_id.band27.tif
if [ $nscans -ge 5 ]; then
  $DBVM_HOME/scripts/run_modis_hdflook.bash $mod021km_file $DBVM_HOME/scripts/template_band02.txt $pass_id.band02.tif
  $DBVM_HOME/scripts/run_modis_hdflook.bash $mod021km_file $DBVM_HOME/scripts/template_truecolor.txt $pass_id.truecolor.tif
fi
echo "(Finishing MODIS image processing)"
fi

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished MODIS Level 1B products at "$(date -u)

# Clean up
rm $file_l1a eoslzx-tmp.* $mod021km_file $mod03_file *.tif.met aqua.att aqua.eph $ir_file $vis_file &> /dev/null
if [ $nscans -lt 5 ]; then rm $pass_id.ch17.jpg; fi &> /dev/null

exit 0
