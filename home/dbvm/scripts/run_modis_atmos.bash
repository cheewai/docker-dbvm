#!/bin/bash

# Script for MODIS Level 2 atmosphere processing (IMAPP)
# Liam.Gumley@ssec.wisc.edu
# 06-APR-2009

echo
echo "Creating MODIS Level 2 atmosphere products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------
# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_modis_atmos.bash file_1km"
  echo "where"
  echo "file_1km is the full path and name of the input MODIS Level 1B 1KM HDF file"
  exit 1
fi

# Get input L1B 1KM file name
file_1km=$1
echo "Input Level 1B 1KM file: "$file_1km

# Check for input L1B 1KM file
if [ ! -r $file_1km ]; then
  echo "Input L1B 1KM file not found: "$file_1km
  exit 1
fi

#-------------------------------------------------------------------------------
# Run IMAPP atmosphere product processing
#-------------------------------------------------------------------------------
echo
echo "(Starting IMAPP atmosphere product processing)"
export MODIS_L2_HOME=$DBVM_HOME/apps/imapp_modisl2
source $DBVM_HOME/scripts/env_imapp.bash
sat_id=$(basename $file_1km | cut -c1-2)
if [ "$sat_id" == "t1" ]; then sat_name="terra"; else sat_name="aqua"; fi
echo modis_level2.csh $sat_name $file_1km $PWD
modis_level2.csh $sat_name $file_1km $PWD
if [ $? -ne 0 ]; then
  echo "IMAPP processing failed for input file: "$file_1km
  exit 1
fi
echo "(Finishing IMAPP atmosphere product processing)"

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished MODIS Level 2 atmosphere products at "$(date -u)
exit 0

