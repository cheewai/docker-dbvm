#!/bin/bash

# Script to convert a DBVM filename (e.g., t1.09001.1535.1000m.hdf) to a date (2009_01_01_001)

# Check arguments
if [ $# -ne 1 ]; then
  echo "Usage: passid_to_date.bash file"
  echo
  echo "where passid is a DBVM filename (e.g., t1.09001.1535.1000m.hdf)"
 exit 1
fi

# Get arguments
filename=$1

# Get year and days of year (e.g., 09 and 001)
year="20"$(basename $filename | cut -c4-5)
jday=$(basename $filename | cut -c6-8)

# Convert to date string (e.g., 2009_01_01_001)
date -d $year"0101 + "$jday" days - 1 day" +%Y_%m_%d_%j

exit 0
