#!/bin/bash

# Script for MODIS Level 2 land processing (MOD14, MODLST, CREFL, NDVI/EVI)
# Liam.Gumley@ssec.wisc.edu
# 06-APR-2009

echo
echo "Creating MODIS Level 2 land products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------
# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_modis_land.bash file_1km "
  echo "where"
  echo "file_1km is the full path and name of the input MODIS Level 1B 1KM HDF file"
  exit 1
fi

# Get input L1B 1KM file name and directory
file_1km=$1
dir_1km=$(dirname $file_1km)
echo
echo "Input Level 1B 1KM file: "$file_1km

# Check for input L1B 1KM file
if [ ! -r $file_1km ]; then
  echo "Input L1B 1KM file not found: "$file_1km
  exit 1
fi

# Get satellite/date/time (e.g., a1.06260.2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Get pass year, day of year, time (e.g, 06 260 2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
year=$(basename $file_1km | cut -c4-5)
jday=$(basename $file_1km | cut -c6-8)
time=$(basename $file_1km | cut -c10-13)

# Get HKM, QKM, and GEO file names
file_hkm=$dir_1km/$date_time.500m.hdf
file_qkm=$dir_1km/$date_time.250m.hdf
file_geo=$dir_1km/$date_time.geo.hdf

#-------------------------------------------------------------------------------
# Run MOD14 fire detection processing
#-------------------------------------------------------------------------------
echo
echo "(Starting MOD14 fire detection processing)"
file_out=$date_time.mod14.hdf
echo $DBVM_HOME/apps/mod14/mod14 -tvC $file_1km $file_geo $file_out
$DBVM_HOME/apps/mod14/mod14 -tvC $file_1km $file_geo $file_out
if [ $? -ne 0 ]; then
  echo "MOD14 processing failed for input file: "$file_1km
  exit 1
fi
echo "(Finishing MOD14 processing)"

#-------------------------------------------------------------------------------
# Run MODLST land surface temperature processing
#-------------------------------------------------------------------------------
echo
echo "(Starting MODLST land surface temperature processing)"
file_out=$date_time.modlst.hdf
export CLIMATOLPATH=$DBVM_HOME/apps/modlst/ancillary/climatology
export LANDCOVERPATH=$DBVM_HOME/apps/modlst/ancillary/landcover
sat_id=$(basename $file_1km | cut -c1-2)
if [ "$sat_id" = "t1" ]; then sat_name="TERRA"; else sat_name="AQUA"; fi
echo $DBVM_HOME/apps/modlst/modlst -v in02=$file_1km in03=$file_geo of=$file_out sat=$sat_name
$DBVM_HOME/apps/modlst/modlst -v in02=$file_1km in03=$file_geo of=$file_out sat=$sat_name
if [ $? -ne 0 ]; then
  echo "MODLST processing failed for input file: "$file_1km
  exit 1
fi
echo "(Finishing MODLST processing)"

#-------------------------------------------------------------------------------
# Run MODIS image processing
#-------------------------------------------------------------------------------
if [ "$DBVM_IMAGE" == "YES" ]; then
echo
echo "(Starting MODIS image processing)"
modlst_file=MODLST.A20$year$jday.$time.hdf
mod03_file=MOD03.A20$year$jday.$time.hdf
ln -f -s $file_out $modlst_file
ln -f -s $file_geo $mod03_file
$DBVM_HOME/scripts/run_modis_hdflook.bash $modlst_file $DBVM_HOME/scripts/template_lst.txt $date_time.modlst.tif
rm $modlst_file $mod03_file
echo "(Finishing MODIS image processing)"
fi

#-------------------------------------------------------------------------------
# CHECK FOR DAY MODE
#-------------------------------------------------------------------------------
echo
echo "(Checking for daytime mode)"
# If daytime data was not found, then exit
if [ ! -r $file_hkm ]; then
  echo "MODIS L1B HKM data was not found; daytime products will not be generated"
  echo "Finished MODIS Level 2 land products at "$(date -u)
  exit 0
else
  echo "MODIS L1B HKM data was found; daytime products will be generated"
fi

#-------------------------------------------------------------------------------
# Run CREFL corrected reflectance processing
#-------------------------------------------------------------------------------
echo
echo "(Starting CREFL corrected reflectance processing)"
export ANCPATH=$DBVM_HOME/apps/crefl
ref_1km=$date_time.crefl.1000m.hdf
ref_hkm=$date_time.crefl.500m.hdf
ref_qkm=$date_time.crefl.250m.hdf
ln -fs $file_1km MOD021KM.tmp.hdf
ln -fs $file_hkm MOD02HKM.tmp.hdf
ln -fs $file_qkm MOD02QKM.tmp.hdf
echo $DBVM_HOME/apps/crefl/crefl.1.4.2 -f -v -1km  MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf MOD021KM.tmp.hdf -of=$ref_1km -bands=1,2,3,4,5,6,7
$DBVM_HOME/apps/crefl/crefl.1.4.2 -f -v -1km  MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf MOD021KM.tmp.hdf -of=$ref_1km -bands=1,2,3,4,5,6,7
if [ $? -ne 0 ]; then
  echo "CREFL 1KM processing failed for input file: "$file_1km
  exit 1
fi
echo $DBVM_HOME/apps/crefl/crefl.1.4.2 -f -v -500m MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf MOD021KM.tmp.hdf -of=$ref_hkm -bands=1,2,3,4,5,6,7
$DBVM_HOME/apps/crefl/crefl.1.4.2 -f -v -500m MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf MOD021KM.tmp.hdf -of=$ref_hkm -bands=1,2,3,4,5,6,7
if [ $? -ne 0 ]; then
  echo "CREFL HKM processing failed for input file: "$file_1km
  exit 1
fi
echo $DBVM_HOME/apps/crefl/crefl.1.4.2 -f -v -250m MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf MOD021KM.tmp.hdf -of=$ref_qkm -bands=1,2
$DBVM_HOME/apps/crefl/crefl.1.4.2 -f -v -250m MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf MOD021KM.tmp.hdf -of=$ref_qkm -bands=1,2
if [ $? -ne 0 ]; then
  echo "CREFL QKM processing failed for input file: "$file_1km
  exit 1
fi
rm MOD021KM.tmp.hdf MOD02HKM.tmp.hdf MOD02QKM.tmp.hdf
echo "(Finishing CREFL processing)"

#-------------------------------------------------------------------------------
# Run NDVI/EVI vegetation index processing
#-------------------------------------------------------------------------------
echo
echo "(Starting NDVI/EVI  vegetation index processing)"
ndvi_1km=$date_time.ndvi.1000m.hdf
ndvi_hkm=$date_time.ndvi.500m.hdf
ndvi_qkm=$date_time.ndvi.250m.hdf
echo $DBVM_HOME/apps/ndvievi/ndvi_evi.2.2 -of=$ndvi_1km -blue=3 -red=1 -nir=2 $ref_1km
$DBVM_HOME/apps/ndvievi/ndvi_evi.2.2 -of=$ndvi_1km -blue=3 -red=1 -nir=2 $ref_1km
if [ $? -ne 0 ]; then
  echo "NDVI/EVI 1KM processing failed for input file: "$file_1km
  exit 1
fi
echo $DBVM_HOME/apps/ndvievi/ndvi_evi.2.2 -of=$ndvi_hkm -blue=3 -red=1 -nir=2 $ref_hkm
$DBVM_HOME/apps/ndvievi/ndvi_evi.2.2 -of=$ndvi_hkm -blue=3 -red=1 -nir=2 $ref_hkm
if [ $? -ne 0 ]; then
  echo "NDVI/EVI HKM processing failed for input file: "$file_1km
  exit 1
fi
echo $DBVM_HOME/apps/ndvievi/ndvi_evi.2.2 -of=$ndvi_qkm -blue=3 -red=1 -nir=2 $ref_qkm
$DBVM_HOME/apps/ndvievi/ndvi_evi.2.2 -of=$ndvi_qkm -blue=3 -red=1 -nir=2 $ref_qkm
if [ $? -ne 0 ]; then
  echo "NDVI/EVI QKM processing failed for input file: "$file_1km
  exit 1
fi
rm $ref_1km $ref_hkm $ref_qkm
echo "(Finishing NDVI/EVI processing)"

#-------------------------------------------------------------------------------
# Run MODIS image processing
#-------------------------------------------------------------------------------
if [ "$DBVM_IMAGE" == "YES" ]; then
echo
echo "(Starting MODIS image processing)"
modndvi_file=MODNDVI.A20$year$jday.$time.hdf
mod03_file=MOD03.A20$year$jday.$time.hdf
ln -f -s $ndvi_1km $modndvi_file
ln -f -s $file_geo $mod03_file
$DBVM_HOME/scripts/run_modis_hdflook.bash $modndvi_file $DBVM_HOME/scripts/template_ndvi.txt $date_time.ndvi.tif
rm $modndvi_file $mod03_file
echo "(Finishing MODIS image processing)"
fi

#-------------------------------------------------------------------------------
# Run MOD09 land surface reflectance processing
#-------------------------------------------------------------------------------
if [ "$DBVM_MOD09" == "YES" ]; then
echo
echo "(Starting MOD09 land surface reflectance processing)"
export MODIS_DB_HOME=$DBVM_HOME/apps/mod09
source $DBVM_HOME/scripts/env_mod09.bash
sat_id=$(basename $file_1km | cut -c1-2)
if [ "$sat_id" == "t1" ]; then sat_name="terra"; else sat_name="aqua"; fi
echo run_mod09.csh $sat_name $file_1km $file_hkm $file_qkm $file_geo 20$year$jday $time
run_mod09.csh $sat_name $file_1km $file_hkm $file_qkm $file_geo 20$year$jday $time
if [ $? -ne 0 ]; then
  echo "MOD09 land surface reflectance processing failed for input file: "$file_1km
  exit 1
fi
if [ "$sat_id" == "t1" ]; then prod_name="MOD09_L2"; else prod_name="MYD09_L2"; fi
mv $prod_name.20$year$jday.$time.hdf $date_time.mod09.hdf
rm MOD09*.hdf MYD09*.hdf
echo "(Finishing MOD09 land surface reflectance processing)"
fi

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished MODIS Level 2 land products at "$(date -u)

exit 0
