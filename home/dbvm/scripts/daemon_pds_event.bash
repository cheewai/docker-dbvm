#!/bin/bash

# Script to look for Level 0 PDS.event files, and process PDS file if found

# NOTES:
# 1. This script is intended to run continuously in the background, i.e., $ daemon_pds_event.bash &
# 2. This script must be restarted if it is changed
# 3. To restart the script, execute restart_daemon_scripts.bash

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$LOG_DIR/daemon_pds_event.log
exec >>$LOG
exec 2>>$LOG

# Set event directory
event_dir=$L0_DIR
cd $event_dir

# Set event script
event_script=db_main.bash

# Start infinite loop
while [ true ]; do

  # Change to event directory
  cd $event_dir

  # Check for .event file
  event_file=$(find . -name "*.event" | head -1)
    
  # If .event file is found, start processing
  if [ -n "$event_file" ]; then
    input_file=$(basename ${event_file%.event})
    proc_file=${event_file%.event}.processing
    mv $event_file $proc_file
    echo "Started processing "$event_dir/$input_file" at "$(date -u)
    $event_script $event_dir/$input_file &
    sleep 30 
    rm $proc_file
  fi
    
  # Wait 30 seconds before checking again
  sleep 30

done

exit 0
