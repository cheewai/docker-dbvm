#!/bin/bash

# Install AIRS Level 1 processing applications

echo
echo "Starting installation of AIRS Level 1 processing applications, please wait..."

echo "(Installing AIRS LEVEL1)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/AIRS_linux_v5.2.tar.gz
tar zxf $DBVM_HOME/tarfiles/AIRS_store_v5.2.tar.gz
mv AIRS airs
cd airs
cp $DBVM_HOME/scripts/noradfile .

echo "(Installing AIRS DEM DATA)"
tar xf $DBVM_HOME/tarfiles/DEM30ARC.tar
tar xf $DBVM_HOME/tarfiles/STD30ARC.tar
gzip -d DEM/*.gz

echo "(Installing AIRS ancillary data)"
cd STORE/ancillary
tar zxf $DBVM_HOME/tarfiles/AIRS_anc_avhrr.tar.gz
tar zxf $DBVM_HOME/tarfiles/AIRS_anc_myd11.tar.gz

echo "Finished installation of AIRS Level 1 processing applications"
exit 0
