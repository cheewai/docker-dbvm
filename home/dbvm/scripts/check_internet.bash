#!/bin/bash

# Script to check internet connection to SSEC
#
#
# NOTE: This script is intended to run via crontab,
# so the DBVM setup script must be run first (see below).

# Run the DBVM setup script (DBVM_HOME is set in the crontab file)
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$DBVM_HOME/logs/check_internet.log
exec >>$LOG
exec 2>>$LOG
echo "________________________"
echo $0 started at `date -u`

# Check internet connection to SSEC
lftp -c "open ftp.ssec.wisc.edu; ls" &> /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: Internet connection to SSEC is down"
  exit 1
fi

exit 0
