#!/bin/bash

# Update the leapsec.dat and utcpole.dat files in the DBVM apps directory tree.
#
# NOTE: This script is intended to run via crontab,
# so the DBVM setup script must be run first (see below).

# Run the DBVM setup script (DBVM_HOME is set in the crontab file)
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$LOG_DIR/update_leapsec_utcpole.log
exec >>$LOG
exec 2>>$LOG
echo "________________________"
echo $0 started at `date -u`

# Switch to apps directory
cd $DBVM_HOME/apps

# Update leapsec.dat file
curl -O $DBVM_LEAPSEC
if [ $? -eq 0 ]; then
  find . -name leapsec.dat -exec rsync leapsec.dat {} \;
  echo "leapsec.dat updated"
  head -v -n 1 leapsec.dat
else
  echo "ERROR: Could not update leapsec.dat at "$(date -u)
  echo "Will create a dummy leapsec.dat in "$DBVM_HOME/apps
  mv leapsec.dat leapsec.dat.old
  leapsec_header.bash > leapsec.dat
  cat leapsec.dat.old | grep -v Checked >> leapsec.dat
fi

# Pause for a few moments so server is not oversubscribed
sleep 5

# Update utcpole.dat file
curl -O $DBVM_UTCPOLE
if [ $? -eq 0 ]; then
  find . -name utcpole.dat -exec rsync utcpole.dat {} \;
  echo "utcpole.dat updated"
  head -v -n 1 utcpole.dat
else
  echo "ERROR: Could not update utcpole.dat at "$(date -u)
  echo "Will create a dummy utcpole.dat in "$DBVM_HOME/apps
  mv utcpole.dat utcpole.dat.old
  utcpole_header.bash > utcpole.dat
  cat utcpole.dat.old | grep -v File | grep -v MJD >> utcpole.dat
fi

exit 0
