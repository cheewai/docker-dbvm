#!/bin/bash

# Install MODIS Level 1 processing applications

echo
echo "Starting installation of MODIS Level 1 processing applications, please wait..."

echo "(Installing EOSLZX)"
cd $DBVM_HOME/apps
mkdir eoslzx
cd eoslzx
unzip -q $DBVM_HOME/tarfiles/eoslzx-free-223.zip

echo "(Installing GBAD)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/GBAD2.6_SPA.tar.gz
mkdir gbad
mv SPA/gbad/algorithm/* gbad
rm -rf SPA

echo "(Installing DESTRIPING)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/DESTRIPE_V1.1.tar.gz

echo "(Installing DESTRIPING patches)"
rsync -aC $DBVM_HOME/scripts/patches/destripe $DBVM_HOME/apps

echo "(Installing MODISL1DB)"
cd $DBVM_HOME/apps
mkdir modisl1db
cd modisl1db
tar zxf $DBVM_HOME/tarfiles/modisl1db_linux.tar.gz
if [ "$DBVM_DEM" == "YES" ]; then
  echo "(Installing MODISL1DB DEM data)"
  tar zxf $DBVM_HOME/tarfiles/seadas_dem_modis.tar.gz
fi

echo "Finished installation of MODIS Level 1 processing applications"
exit 0
