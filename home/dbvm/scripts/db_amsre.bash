#!/bin/bash

# Driver script for AMSR-E DB processing (calls run_amsre.bash)

# Get input AMSR-E APID 402 L0 PDS file name (e.g., P1540402AAAAAAAAAAAAAA10034194351001.PDS)
pds_file=$1

# Get input MODIS L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
file_1km=$2

# Get satellite/date/time (e.g., a1.06260.2057)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Switch to work directory
start_dir=$PWD
temp_dir=$WORK_DIR/$date_time.amsr.level1
mkdir $temp_dir &>/dev/null
cd $temp_dir
if [ $? -ne 0 ]; then
  echo "Could not switch to work directory: "$temp_dir
  exit 1
fi

# Set up logfile and errfile
LOG=$LOG_DIR/$date_time/$date_time.amsr.level1.log
ERR=$LOG_DIR/$date_time/$date_time.amsr.level1.err
exec 1>$LOG 2>$ERR

#--------------------------------------------------------------------------------
# AMSR-E Processing
#--------------------------------------------------------------------------------

# Run AMSR-E processing
echo run_amsre.bash $pds_file $date_time
run_amsre.bash $pds_file $date_time
if [ $? -ne 0 ]; then
  echo "AMSR-E processing failed for input file: "$pds_file
  cd $start_dir
  mv $temp_dir $ERROR_DIR
  mv $LOG $ERROR_DIR
  mv $ERR $ERROR_DIR
  exit 1
fi

# Copy products to archive (note: AMSR-E HDF products are small, so they are not comnpressed)
if [ "$DBVM_ARCHIVE" == "YES" ]; then

  echo
  echo "(Copying products to archive in "$DBVM_ARCHIVE_DIR")"
  echo

  # Get archive directory
  if [ $(basename $file_1km | cut -c1-2) == "t1" ]; then sat_name="terra"; else sat_name="aqua"; fi
  date_string=$(passid_to_date.bash $file_1km)

  # Copy the product files to archive
  rsync -av $date_time*.dat $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level1
  rsync -av AMSR*.hdf $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level2

fi

# Move product files
mv $date_time*.dat $L1_DIR
mv AMSR*.hdf $L2_DIR

# Clean up
cd $start_dir
rm -rf $temp_dir

# Exit
exit 0
