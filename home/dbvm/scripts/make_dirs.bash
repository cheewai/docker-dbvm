#!/bin/bash

# Create DBVM working directories

mkdir $DBVM_HOME/apps
mkdir $DBVM_HOME/errors
mkdir $DBVM_HOME/logs
mkdir $DBVM_HOME/tarfiles
mkdir $DBVM_HOME/data
mkdir $DBVM_HOME/data/ancillary
mkdir $DBVM_HOME/data/ancillary/imapp_modisl2
mkdir $DBVM_HOME/data/ancillary/seadas
mkdir $DBVM_HOME/data/ancillary/mod09
mkdir $DBVM_HOME/data/images
mkdir $DBVM_HOME/data/level0
mkdir $DBVM_HOME/data/level1
mkdir $DBVM_HOME/data/level2
mkdir $DBVM_HOME/data/work
mkdir $DBVM_HOME/data/kml
mkdir $DBVM_HOME/data/tiles
