#!/bin/bash

# Install MODIS Land processing applications

echo
echo "Starting installation of MODIS Land processing applications, please wait..."

echo "(Installing CREFL and NDVI/EVI)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/NDVIEVI2.2_SPA.tar.gz
mv SPA/crefl/algorithm crefl
mv SPA/ndvievi/algorithm ndvievi
rm -rf SPA

echo "(Installing MOD14)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/MOD14_5.0.1_SPA.tar.gz
mv SPA/mod14/algorithm mod14
rm -rf SPA

echo "(Installing MODLST)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/MODLST_4.14_SPA.tar.gz
mv SPA/MODLST/algorithm modlst
rm -rf SPA

if [ "$DBVM_MOD09" == "YES" ]; then

  echo "(Installing MOD09)"
  cd $DBVM_HOME/apps
  tar zxf $DBVM_HOME/tarfiles/MOD09_5.3.18_SPA.tar.gz
  mv SPA/mod09/algorithm mod09
  rm -rf SPA
  rm -rf mod09/DRLshellscripts

  echo "(Installing MOD09 patches)"
  rsync -aC $DBVM_HOME/scripts/patches/mod09 $DBVM_HOME/apps

fi

echo "Finished installation of MODIS Land processing applications"
exit 0
