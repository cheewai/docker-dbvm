#!/bin/bash

# Driver script for MODIS DB Google Earth processing (calls run_modis_dbge.bash)
# Liam.Gumley@ssec.wisc.edu
# 06-MAY-2009

# Get input L1B 1KM file name
file_1km=$1

# Get satellite/date/time (e.g., a1.06260.2057)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Switch to work directory
start_dir=$PWD
temp_dir=$WORK_DIR/$date_time.modis.dbge
mkdir $temp_dir &>/dev/null
cd $temp_dir
if [ $? -ne 0 ]; then
  echo "Could not switch to work directory: "$temp_dir
  exit 1
fi

# Set up logfile and errfile
LOG=$LOG_DIR/$date_time/$date_time.modis.dbge.log
ERR=$LOG_DIR/$date_time/$date_time.modis.dbge.err
exec 1>$LOG 2>$ERR

#--------------------------------------------------------------------------------
# MODIS Level 2 Google Earth Processing
#--------------------------------------------------------------------------------

# Run MODIS Level 2 Google Earth Processing (note that products are stored in $DBVM_HOME/kml)
echo run_modis_dbge.bash $file_1km
run_modis_dbge.bash $file_1km
if [ $? -ne 0 ]; then
  echo "Level 2 Google Earth processing failed for input file: "$file_1km
  cd $start_dir
  mv $temp_dir $ERROR_DIR
  mv $LOG $ERROR_DIR
  mv $ERR $ERROR_DIR
  exit 1
fi

# Copy products to archive
if [ "$DBVM_ARCHIVE" == "YES" ]; then

  echo
  echo "(Copying products to archive in "$DBVM_ARCHIVE_DIR")"
  echo

  # Copy the product files to archive
  rsync -a $DATA_DIR/kml $DBVM_ARCHIVE_DIR

fi

# Clean up
cd $start_dir
rm -rf $temp_dir

# Exit
exit 0
