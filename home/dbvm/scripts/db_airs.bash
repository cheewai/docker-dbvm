#!/bin/bash

# Driver script for AIRS DB processing (calls run_airs.bash)

# Get input AIRS APID 404 L0 PDS file name (e.g., P1540404AAAAAAAAAAAAAA10034194351001.PDS)
pds_file=$1

# Get input MODIS L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
file_1km=$2

# Get satellite/date/time (e.g., a1.06260.2057)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Switch to work directory
start_dir=$PWD
temp_dir=$WORK_DIR/$date_time.airs
mkdir $temp_dir &>/dev/null
cd $temp_dir
if [ $? -ne 0 ]; then
  echo "Could not switch to work directory: "$temp_dir
  exit 1
fi

# Set up logfile and errfile
LOG=$LOG_DIR/$date_time/$date_time.airs.log
ERR=$LOG_DIR/$date_time/$date_time.airs.err
exec 1>$LOG 2>$ERR

#--------------------------------------------------------------------------------
# AIRS Processing
#--------------------------------------------------------------------------------

# Run AIRS processing
echo run_airs.bash $pds_file
run_airs.bash $pds_file
if [ $? -ne 0 ]; then
  echo "AIRS Level 1 processing failed for input file: "$pds_file
  cd $start_dir
  mv $temp_dir $ERROR_DIR
  mv $LOG $ERROR_DIR
  mv $ERR $ERROR_DIR
  exit 1
fi

# Copy products to archive (note: AIRS HDF products are compressed by default)
if [ "$DBVM_ARCHIVE" == "YES" ]; then

  echo
  echo "(Copying products to archive in "$DBVM_ARCHIVE_DIR")"
  echo

  # Get archive directory
  if [ $(basename $file_1km | cut -c1-2) == "t1" ]; then sat_name="terra"; else sat_name="aqua"; fi
  date_string=$(passid_to_date.bash $file_1km)

  # Copy the product files to archive
  rsync -av AIRS.*.L1B.*Rad*.hdf $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level1
  rsync -av AIRS.*.L2.*.hdf $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level2

fi

# Move product files
mv AIRS.*.L1B.*Rad*.hdf $L1_DIR
mv AIRS.*.L2.*.hdf $L2_DIR

# Clean up
cd $start_dir
rm -rf $temp_dir

# Exit
exit 0
