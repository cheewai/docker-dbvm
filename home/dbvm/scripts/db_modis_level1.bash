#!/bin/bash

# Driver script for MODIS DB Level 1 processing (calls run_modis_level1.bash)

# Get input MODIS APID 64 L0 PDS file name (e.g., P1540064AAAAAAAAAAAAAA10034194351001.PDS)
pds_file=$1

# Switch to work directory
start_dir=$PWD
temp_dir=$WORK_DIR/$(basename $pds_file).modis.level1
mkdir $temp_dir &>/dev/null
cd $temp_dir
if [ $? -ne 0 ]; then
  echo "Could not switch to work directory: "$temp_dir
  exit 1
fi

# Set up logfile and errfile
LOG=$LOG_DIR/$(basename $pds_file).modis.level1.log
ERR=$LOG_DIR/$(basename $pds_file).modis.level1.err
exec 1>$LOG 2>$ERR

#--------------------------------------------------------------------------------
# MODIS Level 1 Processing
#--------------------------------------------------------------------------------

# Run MODIS Level 1 processing
echo run_modis_level1.bash $pds_file
run_modis_level1.bash $pds_file
if [ $? -ne 0 ]; then
  echo "MODIS Level 1 processing failed for input file: "$pds_file
  cd $start_dir
  mv $temp_dir $ERROR_DIR
  mv $LOG $ERROR_DIR
  mv $ERR $ERROR_DIR
  exit 1
fi

# Write L1B 1KM HDF file name to text file
file_1km=$(ls -1tr *.1000m.hdf | tail -1)
text_file=$(basename $pds_file).modis.level1
echo $file_1km > $text_file

# Copy products to archive
if [ "$DBVM_ARCHIVE" == "YES" ]; then

  echo
  echo "(Copying products to archive in "$DBVM_ARCHIVE_DIR")"
  echo

  # Get archive directory
  if [ $(basename $pds_file | cut -c1-4) == "P042" ]; then sat_name="terra"; else sat_name="aqua"; fi
  date_string=$(passid_to_date.bash $file_1km)

  # Create archive directories
  mkdir -p $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level0 &> /dev/null
  mkdir -p $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level1 &> /dev/null
  mkdir -p $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level2 &> /dev/null
  mkdir -p $DBVM_ARCHIVE_DIR/$sat_name/$date_string/images &> /dev/null

  # Copy HDF product files to archive (compress if required)
  archive_dir=$DBVM_ARCHIVE_DIR/$sat_name/$date_string/level1
  if [ "$DBVM_COMPRESS" == "YES" ]; then
    for file in *.hdf; do hrepack.exe -i $file -o $archive_dir/$file -t '*:GZIP 1'; done
  else
    rsync -av *.hdf $archive_dir
  fi

  # Copy other product files to archive
  pds_dir=$(dirname $pds_file)
  pds_string="*"$(basename $pds_file | cut -c9-34)"01.PDS"
  rsync -av $pds_dir/$pds_string $DBVM_ARCHIVE_DIR/$sat_name/$date_string/level0
  rsync -av *.jpg $DBVM_ARCHIVE_DIR/$sat_name/$date_string/images
  rsync -av *.tif $DBVM_ARCHIVE_DIR/$sat_name/$date_string/images

fi

# Move product files
mv *.hdf $text_file $L1_DIR
mv *.jpg $IMAGE_DIR
mv *.tif $IMAGE_DIR

# Clean up
cd $start_dir
rm -rf $temp_dir

# Exit
exit 0
