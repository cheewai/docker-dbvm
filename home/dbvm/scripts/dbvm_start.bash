#!/bin/bash

# Start the DBVM system

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

echo "(Starting the DBVM system)"

echo "(Starting daemons)"
restart_daemon_scripts.bash

echo "(Activating crontab)"
if [ ! -f $DBVM_HOME/scripts/crontab.txt ]; then
  echo "ERROR: DBVM crontab file does not exist. Run install_dbvm.bash to create it."
  exit 1
fi
crontab $DBVM_HOME/scripts/crontab.txt

echo "(Updating leapsec.dat and utcpole.dat files)"
update_leapsec_utcpole.bash

exit 0
