#!/bin/bash

# Process MODIS L1B data for KML tile generation
#
# Usage: process_granule.bash file_1km file_hkm file_qkm file_geo satname juldate utctime
#
# Liam.Gumley@ssec.wisc.edu

# Startup message
echo
echo "Started "$0" at "`date`

# Check for DBGE_HOME environment variable
if [ -z "$DBGE_HOME" ]; then
  echo "Environment variable DBGE_HOME is not set"
  exit 1
fi

# Echo command line
echo
echo "(Echoing command line)"
echo $0 $@

# Check number of arguments
if [ $# -ne 7 ]; then
  echo
  echo "Usage: process_granule.bash file_1km file_hkm file_qkm file_geo satname juldate utctime"
  echo "where"
  echo "file_1km = MODIS L1B 1000 meter HDF file"
  echo "file_hkm = MODIS L1B 500 meter HDF file"
  echo "file_qkm = MODIS L1B 250 meter HDF file"
  echo "file_geo = MODIS L1B geolocation HDF file"
  echo "satname = Satellite name (terra or aqua)"
  echo "juldate = Julian date in format yyyyddd where yyyy is year and ddd is day of year (e.g., 2008218)"
  echo "utctime = UTC time in format hhmm at start of granule (e.g. 1835)"
  exit 1
fi

# Get arguments
file_1km=$1
file_hkm=$2
file_qkm=$3
file_geo=$4
satname=$5
juldate=$6
utctime=$7

# Check if arguments exist
if [ ! -r $file_1km ]; then echo "File cannot be read: "$file_1km; exit 1; fi
if [ ! -r $file_hkm ]; then echo "File cannot be read: "$file_hkm; exit 1; fi
if [ ! -r $file_qkm ]; then echo "File cannot be read: "$file_qkm; exit 1; fi
if [ ! -r $file_geo ]; then echo "File cannot be read: "$file_geo; exit 1; fi

# Check if HDF files are valid
result=$($DBGE_HOME/bin/ncdump -h $file_1km | grep "short EV_1KM_Emissive")
if [ $? -ne 0 ]; then echo "File is not valid MODIS L1B 1000 meter HDF: "$file_1km; exit 1; fi
result=$($DBGE_HOME/bin/ncdump -h $file_hkm | grep "short EV_500_RefSB")
if [ $? -ne 0 ]; then echo "File is not valid MODIS L1B 500 meter HDF: "$file_hkm; exit 1; fi
result=$($DBGE_HOME/bin/ncdump -h $file_qkm | grep "short EV_250_RefSB")
if [ $? -ne 0 ]; then echo "File is not valid MODIS L1B 250 meter HDF: "$file_qkm; exit 1; fi
result=$($DBGE_HOME/bin/ncdump -h $file_geo | grep "byte Land/SeaMask")
if [ $? -ne 0 ]; then echo "File is not valid MODIS L1B geolocation HDF: "$file_geo; exit 1; fi

# Check if satellite name, date, time are valid
if [ $satname != "terra" ] && [ $satname != "aqua" ]; then echo "Satellite name is not valid: "$satname; exit 1; fi
if [ $juldate -lt 2000001 ] || [ $juldate -gt 2100001 ]; then echo "Julian date is not valid: "$juldate; exit 1; fi
if [ $utctime -lt 0000 ] || [ $utctime -gt 2359 ]; then echo "UTC time is not valid: "$utctime; exit 1; fi

#-------------------------------------------------------------------------------
# CORRECTED REFLECTANCE PROCESSING
#-------------------------------------------------------------------------------

# Check if this is a daytime granule
echo
echo "(Checking for daytime granule)"
nscans=$($DBGE_HOME/bin/ncdump -h $file_1km | grep "Number of Day mode scans" | awk '{print $7}')
echo "Number of day mode scans = "$nscans
if [ $nscans -ge 5 ]; then
  echo "Granule has 5 or more day mode scans; processing will continue"
else
  echo "Granule has less than 5 day mode scans: processing will exit"
  exit 1
fi

# Create link to geolocation file
geo_1km="MOD03".$juldate.$utctime.tmp.hdf
ln -f -s $file_geo $geo_1km

# Set corrected reflectance output filenames
ref_1km="MOD021KM".$juldate.$utctime.crefl.hdf
ref_hkm="MOD02HKM".$juldate.$utctime.crefl.hdf
ref_qkm="MOD02QKM".$juldate.$utctime.crefl.hdf

# Create corrected reflectance
echo
echo "(Creating MODIS corrected reflectance 1KM, HKM, QKM)"
if [ "$DBGE_DEBUG" == "YES" ]; then
  echo run_modis_crefl.bash $file_1km $file_hkm $file_qkm $ref_1km $ref_hkm $ref_qkm
  run_modis_crefl.bash $file_1km $file_hkm $file_qkm $ref_1km $ref_hkm $ref_qkm
else
  run_modis_crefl.bash $file_1km $file_hkm $file_qkm $ref_1km $ref_hkm $ref_qkm &> /dev/null
fi

#-------------------------------------------------------------------------------
# TILE IMAGE PROCESSING
#-------------------------------------------------------------------------------

# Generate event files (e.g., tile412/tile412.terra.2008190.1415.event) for tiles covered by this granule
echo
echo "(Finding tiles covered by this granule)"
if [ "$DBGE_DEBUG" == "YES" ]; then
  echo run_modis_metadata.bash $file_geo $satname $juldate $utctime $DBGE_TILE_DIR
  run_modis_metadata.bash $file_geo $satname $juldate $utctime $DBGE_TILE_DIR
else
  run_modis_metadata.bash $file_geo $satname $juldate $utctime $DBGE_TILE_DIR &> /dev/null
fi

# Get list of tiles
event_list=$(find $DBGE_TILE_DIR -name "tile*.$satname.$juldate.$utctime.event" | sort)

# Process each tile covered by this granule
echo
echo "------------------------------------------"
echo "(Processing tiles covered by this granule)"
for event_file in $event_list; do

  echo
  echo "(Processing "$(basename $event_file)")"

  # Get tile number (e.g., tile442)
  tilenum=$(basename $event_file | cut -d. -f1)
  
  # Get metadata file name (e.g., tile442/tile442.terra.2008218.1825.dat)
  meta_file=$DBGE_TILE_DIR/$tilenum/$tilenum.$satname.$juldate.$utctime.dat
  
  # Get OUL (output uppper left) and OLR (output lower right) lon/lat data from metadata file
  oul=$(grep OUL $meta_file | awk '{print $3$4}')
  olr=$(grep OLR $meta_file | awk '{print $3$4}')

  # Generate reprojected TIFF image for this tile (e.g., tile442/tile442.terra.2008218.1825.tif)
  echo "(Reprojecting MODIS data for this tile)"
  image_file=$DBGE_TILE_DIR/$tilenum/$tilenum.$satname.$juldate.$utctime.tif
  if [ "$DBGE_DEBUG" == "YES" ]; then
    echo run_modis_reproject.bash $file_geo $ref_hkm $ref_qkm $oul $olr $image_file
    run_modis_reproject.bash $file_geo $ref_hkm $ref_qkm $oul $olr $image_file
  else
    run_modis_reproject.bash $file_geo $ref_hkm $ref_qkm $oul $olr $image_file &> /dev/null
  fi
  
  # Composite all TIF images for this tile into one output TIFF (e.g., tile442/tile442.terra.2008218.tif)
  echo "(Compositing image files)"
  comp_file=$tilenum.$satname.$juldate.tif
  echo "cd, '"$DBGE_TILE_DIR/$tilenum"'" > runidl
  echo "list = file_search('"$tilenum.$satname.$juldate".????.tif')" >> runidl
  echo "help, list" >> runidl
  echo "print, list" >> runidl
  echo "outfile = '"$comp_file"'" >> runidl
  echo "image_composite, list, outfile" >> runidl
  echo "exit" >> runidl
  if [ "$DBGE_DEBUG" == "YES" ]; then
    cat runidl
    echo $IDL_CMD runidl
    $IDL_CMD runidl
  else
    $IDL_CMD runidl &> /dev/null
  fi

  # Convert TIF composite image to JPEG composite
  echo "(Converting composite TIFF to JPEG)"
  jpeg_file=$tilenum.$satname.$juldate.jpg
  if [ "$DBGE_DEBUG" == "YES" ]; then
    echo "(cd $DBGE_TILE_DIR/$tilenum; convert -sigmoidal-contrast 3,50% -modulate 100,125,100 -gamma 1.1 -unsharp 0 -quality 90 $comp_file $jpeg_file)"
  fi
  (cd $DBGE_TILE_DIR/$tilenum; convert -sigmoidal-contrast 3,50% -modulate 100,125,100 -gamma 1.1 -unsharp 0 -quality 90 $comp_file $jpeg_file)

done

#-------------------------------------------------------------------------------
# CREATE "LAST UPDATE" IMAGE
#-------------------------------------------------------------------------------

# Set file name and capitalized satellite name
update_file=$DBGE_KML_DIR/$satname.$juldate.update.jpg
if [ "$satname" == "terra" ]; then nicename="Terra"; else nicename="Aqua"; fi

# Create date string in yyyy/mm/dd format
year=$(echo $juldate | cut -c1-4)
jday=$(echo $juldate | cut -c5-7)
date_string=$(date --date "$year/01/01 + $jday days - 1 day" +%Y"/"%m"/"%d)

# Create image
label_string=$nicename" MODIS "$date_string"; last update "$utctime" UTC"
if [ "$DBGE_DEBUG" == "YES" ]; then
  echo "convert -pointsize 16 -fill white -background black label:"$label_string" $update_file"
fi
convert -pointsize 16 -fill white -background black label:"$label_string" $update_file

#-------------------------------------------------------------------------------
# KML PROCESSING
#-------------------------------------------------------------------------------

# Create KML for each tile covered by this granule
echo
echo "------------------------------------------------"
echo "(Creating KML for tiles covered by this granule)"
for event_file in $event_list; do

  # Get tile number (e.g., tile442)
  tilenum=$(basename $event_file | cut -d. -f1)
  
  # Get JPEG composite file name (e.g., tile442/tile442.terra.2008218.jpg)
  jpeg_file=$tilenum.$satname.$juldate.jpg

  # Create KML for this tile
  echo "(Creating KML for "$tilenum")"
  echo "cd, '"$DBGE_KML_DIR"'" > runidl
  echo "make_image_tiles,'"$jpeg_file"'" >> runidl
  echo "exit" >> runidl
  if [ "$DBGE_DEBUG" == "YES" ]; then
    cat runidl
    echo $IDL_CMD runidl
    $IDL_CMD runidl
  else
    $IDL_CMD runidl &> /dev/null
  fi

  # Remove tile event file
  /bin/rm -f $event_file
  
done

# Make top-level kml files
echo "(Making final top-level kml files)"
echo "cd, '"$DBGE_KML_DIR"'" > runidl
echo "make_global_kml,'"$jpeg_file"'" >> runidl
echo "exit" >> runidl
if [ "$DBGE_DEBUG" == "YES" ]; then
  cat runidl
  echo $IDL_CMD runidl
  $IDL_CMD runidl
else
  $IDL_CMD runidl &> /dev/null
fi

# Make user-end kml files
echo "(Making user-end kml files)"
echo "cd, '"$DBGE_KML_DIR"'" > runidl
echo "make_user_kml,'"$DBGE_KML_URL"','"$jpeg_file"',$" >> runidl
echo "  '"$DBGE_SITE_NAME"',"$DBGE_SITE_LAT","$DBGE_SITE_LON >> runidl
echo "exit" >> runidl
if [ "$DBGE_DEBUG" == "YES" ]; then
  cat runidl
  echo $IDL_CMD runidl
  $IDL_CMD runidl
else
  $IDL_CMD runidl &> /dev/null
fi

#-------------------------------------------------------------------------------
# CLEANUP
#-------------------------------------------------------------------------------

# Cleanup and exit
/bin/rm -f $ref_1km $ref_hkm $ref_qkm $geo_1km runidl
/bin/rm -f idlsave.pro flsave.pro mrtswath.log MOD02HKM.hdf MOD02QKM.hdf MOD03.hdf

# Finish message
echo
echo "Finished "$0" at "`date`
exit 0
