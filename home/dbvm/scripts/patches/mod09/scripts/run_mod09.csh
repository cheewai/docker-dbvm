#!/bin/csh -f

# Run MODIS_DB version of MOD_PR09
# Liam.Gumley@ssec.wisc.edu, 2007-09-24

#-------------------------------------------------------------------------------
# SETUP AND CHECK ARGUMENTS
#-------------------------------------------------------------------------------

# Shell setup
unlimit

# Set product name
set PRODUCT = MOD_PR09

# Print start message
echo
echo $PRODUCT" processing started at "`date`

# Check for MODIS_DB_HOME environment variable
if (! ${?MODIS_DB_HOME}) then
  echo "Error: Environment variable MODIS_DB_HOME is not set"
  exit 1
endif

# Check number of arguments
if ($#argv != 7) then
  echo "Usage: run_mod09.csh SAT FIL1KM FILHKM FILQKM FILGEO DATE TIME"
  echo "where"
  echo "SAT    is the satellite name (terra or aqua)"
  echo "FIL1KM is the Level 1B 1000 meter radiance HDF file"
  echo "FILHKM is the Level 1B  500 meter radiance HDF file"
  echo "FILQKM is the Level 1B  250 meter radiance HDF file"
  echo "FILGEO is the Level 1B 1000 meter geolocation HDF file"
  echo "DATE   is the year and day of year in yyyyddd format (e.g., 2009072)"
  echo "TIME   is the UTC time in hours and minutes in hhmm format (e.g., 0715)"
  exit 1
endif

# Echo command line
echo
echo $0 $argv

# Check satellite name
if ($argv[1] != "terra" && $argv[1] != "aqua") then
  echo "Invalid satellite name: "$argv[1]
  exit 1
endif

# Check that input files exist
foreach FILE ($argv[2] $argv[3] $argv[4] $argv[5])
  if (! -e $FILE) then
    echo "Input file not found: "$FILE
    exit 1
  endif
end

#-------------------------------------------------------------------------------
# EXTRACT ARGUMENTS
#-------------------------------------------------------------------------------

# Extract file names and directories
set SAT    = $argv[1]
set FIL1KM = `basename $argv[2]`  
set FILHKM = `basename $argv[3]`  
set FILQKM = `basename $argv[4]`  
set FILGEO = `basename $argv[5]`  
set DIR1KM = `dirname $argv[2]`   
set DIRHKM = `dirname $argv[3]`   
set DIRQKM = `dirname $argv[4]`   
set DIRGEO = `dirname $argv[5]`   
set DATE   = `expr $argv[6] + 0`
set TIME   = `expr $argv[7] + 0`
set DATE_TIME = $argv[6]"."$argv[7]

# Get platform header (MOD or MYD)
set HEADER = "MOD"
if ($SAT == "aqua") set HEADER = "MYD"

# Check date and time
if ($DATE < 2000001 || $DATE > 2020366) then
  echo "Invalid date: "$DATE
  exit 1
endif

set year = `echo $DATE | cut -c1-4`
if ($year < 2000 || $year > 2020) then
  echo "Invalid year in DATE argument: "$DATE
  exit 1
endif

set day = `echo $DATE | cut -c5-7`
set day = `expr $day + 0`
if ($day < 1 || $day > 366) then
  echo "Invalid day of year in DATE argument: "$DATE
  exit 1
endif

if ($TIME < 0 || $TIME > 2359) then
  echo "Invalid time: "$TIME
  exit 1
endif

#-------------------------------------------------------------------------------
# GET GDAS ANCILLARY FILES
#-------------------------------------------------------------------------------

echo
echo "(Getting GDAS or GFS ancillary data for $year day $day time $TIME)"

# Get the correct first numerical weather prediction file
echo
set pre_time = `get_back_or_ahead_time.csh $DATE $TIME BACKWARD`
set forecast_date1=`echo $pre_time | cut -c 1-7`
set forecast_time1=`echo $pre_time | cut -c 8-11`
set GDAS1=`get_anc_gdas_gfs.csh $forecast_date1 $forecast_time1`
if ($GDAS1 == "") then
  echo 
  echo $0" ERROR: "$PRODUCT" processing failed."
  echo "GDAS/GFS ancillary data could not be found for date "$DATE" and time "$TIME
  echo
  exit 1
else
  echo
  echo "GDAS/GFS file 1: "$GDAS1
endif

# Get the correct second numerical weather prediction file
echo
set post_time = `get_back_or_ahead_time.csh $DATE $TIME FORWARD`
set forecast_date2=`echo $post_time | cut -c 1-7`
set forecast_time2=`echo $post_time | cut -c 8-11`
set GDAS2=`get_anc_gdas_gfs.csh $forecast_date2 $forecast_time2`
if ($GDAS2 == "") then
  echo 
  echo $0" ERROR: "$PRODUCT" processing failed."
  echo "GDAS/GFS ancillary data could not be found for date "$DATE" and time "$TIME
  echo
  exit 1
else
  echo
  echo "GDAS/GFS file 2: "$GDAS2
endif

# Get separate path and file names
set FILGDAS1 = `basename $GDAS1`
set FILGDAS2 = `basename $GDAS2`
set DIRGDAS1 = `dirname $GDAS1`
set DIRGDAS2 = `dirname $GDAS2`

#-------------------------------------------------------------------------------
# CREATE PCF FILE
#-------------------------------------------------------------------------------

# Set location of database files
set DATABASE1 = $MODIS_DB_HOME/$PRODUCT/data
set DATABASE2 = $MODIS_DB_HOME/$PRODUCT/data/newLUTs/v3.0

# Set location of MCF (*.mcf) files
set DIRMCF = $MODIS_DB_HOME/$PRODUCT/mcf

# Set location of leapsec.dat and utcpole.dat files
set DIRTKD = $MODIS_DB_HOME/toolkit/data

# Set name of PCF template file
set TEMPLATE = $MODIS_DB_HOME/$PRODUCT/template/${HEADER}_PR09.pcf.template

# Set name of new PCF file
set FILPCF = ${HEADER}_PR09.$DATE_TIME.pcf

# Set names of output files
set FILOUT_L2 = ${HEADER}09_L2.$DATE_TIME.hdf
set FILOUT_CR = ${HEADER}09_CR.$DATE_TIME.hdf
set FILOUT_CMG = ${HEADER}09_CMG.$DATE_TIME

# Set start and stop times (only files within this range will be processed)
set START_TIME = '2000-01-01T00:00:00'
set STOP_TIME = `date -u +%Y-%m-%dT%H:%M:%S`

# Set platform name
set PLATFORM_NAME = `uname -a`

# Create new PCF file from the template
sed \
  -e "s?FIL1KM?${FIL1KM}?g" \
  -e "s?FILHKM?${FILHKM}?g" \
  -e "s?FILQKM?${FILQKM}?g" \
  -e "s?FILGEO?${FILGEO}?g" \
  -e "s?FILGDAS1?${FILGDAS1}?g" \
  -e "s?FILGDAS2?${FILGDAS2}?g" \
  -e "s?DIR1KM?${DIR1KM}?g" \
  -e "s?DIRHKM?${DIRHKM}?g" \
  -e "s?DIRQKM?${DIRQKM}?g" \
  -e "s?DIRGEO?${DIRGEO}?g" \
  -e "s?DIRGDAS1?${DIRGDAS1}?g" \
  -e "s?DIRGDAS2?${DIRGDAS2}?g" \
  -e "s?FILOUT_L2?${FILOUT_L2}?g" \
  -e "s?FILOUT_CR?${FILOUT_CR}?g" \
  -e "s?FILOUT_CMG?${FILOUT_CMG}?g" \
  -e "s?START_TIME?${START_TIME}?g" \
  -e "s?STOP_TIME?${STOP_TIME}?g" \
  -e "s?PLATFORM_NAME?${PLATFORM_NAME}?g" \
  -e "s?DATABASE1?${DATABASE1}?g" \
  -e "s?DATABASE2?${DATABASE2}?g" \
  -e "s?DIRMCF?${DIRMCF}?g" \
  -e "s?DIRTKD?${DIRTKD}?g" \
  $TEMPLATE >! $FILPCF

#-------------------------------------------------------------------------------
# RUN THE 1KM and 500M DESTRIPING ALGORITHMS
#-------------------------------------------------------------------------------

# 1KM destriping
set result = `$MODIS_DB_HOME/scripts/ncdump -h $DIR1KM/$FIL1KM | grep "UW_DESTRIPE_1KM ="`
if ($#result == 0) then
  echo
  echo "(Running destriping algorithm on L1B 1KM file)"
  $MODIS_DB_HOME/scripts/run_modds1km.csh $SAT $DIR1KM/$FIL1KM >& /dev/null
  if ($status != 0) then
    echo
    echo $0" ERROR: Could not run destriping algorithm on L1B 1KM file "$DIR1KM/$FIL1KM
    echo
    exit 1
  endif
endif

# 500M destriping
set result = `$MODIS_DB_HOME/scripts/ncdump -h $DIRHKM/$FILHKM | grep "UW_DESTRIPE_500M ="`
if ($#result == 0) then
  echo
  echo "(Running destriping algorithm on L1B 500M file)"
  $MODIS_DB_HOME/scripts/run_modds500m.csh $SAT $DIRHKM/$FILHKM >& /dev/null
  if ($status != 0) then
    echo
    echo $0" ERROR: Could not run destriping algorithm on L1B 500M file "$DIRHKM/$FILHKM
    echo
    exit 1
  endif
endif

#-------------------------------------------------------------------------------
# RUN THE MOD09 SCIENCE ALGORITHM
#-------------------------------------------------------------------------------

echo
echo "(Running "$PRODUCT" algorithm)"
echo

# Set toolkit environment variables
setenv PGS_PC_INFO_FILE ./$FILPCF
setenv PGSMSG $MODIS_DB_HOME/toolkit/message

# Run the algorithm
$MODIS_DB_HOME/$PRODUCT/bin/MOD_PR09DB.exe
set PGE_STATUS = $status

# Print failure message and exit if needed
if ($PGE_STATUS != 0) then
  echo
  echo $0" ERROR: "$PRODUCT" processing failed."
  echo "Please examine the LogStatus and LogUser files for more information."
  echo
  exit 1
endif

# Clean up
rm -f $FILPCF ShmMem LogUser LogReport LogStatus GetAttr.temp
rm -f leapsec.dat utcpole.dat
rm -f pc* *.met

# Print finish message
echo
echo $0": "$PRODUCT" processing successful"
echo $PRODUCT" processing ended at "`date`
echo
exit 0
