Aqua MODIS band 27 detector 3 became noisy over the South Atlantic 
Anomaly (SAA) on Jan 10, 2005 during granule 1715 GMT. For this and later 
times, the destriping algorithm uses an updated configuration file (*.v2) that 
replaces Channel 3 with one of its neighbors. Data processed prior to the SAA 
event use the original configuration file (*.v1). 

Terra MODIS band 27 detector 3 became noisy on Sept. 9, 2007. From 0000 UTC on
this day forward, it should be flagged for replacement in the MOD_PRDS
destriping code. Granules processed after this time should use destripe_config_terra.dat.v2.

Liam Gumley
26 Feb 2008

At the request of the MOD14 fire detection algorithm developer (Louis Giglio),
destriping for bands 21 and 22 has been disabled.

Liam Gumley
5 August 2008

Destriping for bands 20, 31, and 32 as been disabled for Collection 6.

Liam Gumley
11 March 2010
