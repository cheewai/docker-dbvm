#!/bin/csh -f
#
#---------------  MODIS DESTRIPING RUN SCRIPT --------------------------------
# Run the IMAPP MODIS destriping software on a 1KM MODIS data set.
# This version of the MODIS L1B destriping algorithm is based on the IDL version
# that has been running at CIMSS on direct broadcast since June 2003. The
# algorithm is reliable and fast in the IDL implementation.
#
# The algorithm is based on Weinreb et al., 1989: "Destriping GOES Images by
# Matching Empirical Distribution Functions". Remote Sens. Environ., 29,
# 185-195.
#
# 1. Algorithm Details
# - Accounts for both detector-to-detector and mirror side striping.
# - MODIS is treated as a 20 detector instrument in the thermal emissive bands
#  (10 detectors on each mirror side).
# - The empirical distribution function (EDF) is computed for each detector
#  (cumulative histogram of relative frequency).
# - The EDF for each detector is adjusted to match the EDF of a reference
#   in-family detector.
# - Algorithm operates on L1B scaled integers (0-32767).
# - Median scaled integer value for each band is restored following destriping.
#
# 2. Terra and Aqua MODIS Implementation in FORTRAN-90
# - Correction LUT is created for each individual granule on the fly.
# - Uncorrected scaled integers are replaced with corrected scaled integers.
# - Impact on bands 31 and 32 is equivocal.
# - For Terra MODIS, noisy detectors in some bands are replaced with neighbors.
# - For Aqua MODIS, no detectors are replaced.
# - Requires less than 60 seconds to run for each granule.
# - Text configuration file (one each for Terra and Aqua) defines
#   (a) which bands will be destriped
#   (b) which detector will be the reference for each band
#   (c) which detectors will be replaced for each band
#
# 3. Impact on the L1B 1KM data
# - For most of the thermal infrared bands, the impact is positive. Striping noise 
#   is significantly reduced, and this has a positive impact on downstream 
#   products such as the cloudmask, atmospheric profiles, and cloud top
#   properties.
# - For bands 31 and 32, the impact is equivocal. However, for cloud top phase
#   computations involving differences between bands 31, 32, and 29, the impact
#   appears to be positive.
# - NOTE: The L1B 1KM file is irreversibly changed by this algorithm. It is
#   possible, but complicated, to implement this algorithm in such a way that the
#   destriping is reversible.
#
#
#-------------------------------------------------------------------------------
# SETUP
#-------------------------------------------------------------------------------

# Set environmental variables
#  These variables must be set for correct execution of the destriping software
#setenv DESTRIPE_ROOT  /data2/kathys/destripe
#setenv DESTRIPE_LUT $DESTRIPE_ROOT/coeff
#setenv DESTRIPE_BIN $DESTRIPE_ROOT/bin

# Check arguments
if ($#argv != 2) then
  echo "Usage: run_modis_destripe.csh SAT FIL1KM"
  echo "where"
  echo "SAT is the satellite platform (aqua or terra)"
  echo "FIL1KM is MODIS L1B 1000 meter resolution HDF image file"
  exit(-1)
endif

# Extract file names
set SAT = $argv[1]
set FIL1KM = $argv[2]

# Set root of output file name (e.g. 't1.02001.1815')
set ROOT = $FIL1KM:r
set ROOT = $ROOT:r
set ROOT = $ROOT:t

# Get year and date for ancillary data extraction
set YR     = `echo $FIL1KM:t | cut -c4-5`
set YEAR   = `expr 2000 + $YR`
set DAY    = `echo $FIL1KM:t | cut -c 6-8`
set HOUR   = `echo $FIL1KM:t | cut -c 10-11`
set MINUTE = `echo $FIL1KM:t | cut -c 12-13`

set DATE   =  ${YEAR}${DAY}
set TIME   = ${HOUR}${MINUTE}

# Select the right coefficient file based upon the date of the data
# -----------------------------------------------------------------
if ($SAT == "terra") then
   set data_v2=`${DESTRIPE_BIN}/dateplus.exe -u 20050110`   
else 
   if ($SAT == "aqua") then
     set data_v2=`${DESTRIPE_BIN}/dateplus.exe -u 20070909`
   endif
else
     set data_v2="MISSING"
     echo "Satellite does not match Aqua or Terra " $SAT
endif

set greg_day = `${DESTRIPE_BIN}/dateplus.exe -J ${YEAR}${DAY}`
set data_day = `${DESTRIPE_BIN}/dateplus.exe -u ${greg_day}`

if ($data_day < $data_v2) then
   set DESTRIPE_COEFF = `find ${DESTRIPE_LUT} -name "destripe_config_${SAT}.dat.v1" -print` 
else
   set DESTRIPE_COEFF = `find ${DESTRIPE_LUT} -name "destripe_config_${SAT}.dat.v2" -print` 
endif

echo "Using coefficient file: " $DESTRIPE_COEFF
  
#-------------------------------------------------------------------------------
# Print start message for processing log
echo
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Started MODIS destriping process at "`date`

# Run the destriping algorithm
${DESTRIPE_BIN}/MOD_PRDS_DB.exe $FIL1KM $DESTRIPE_COEFF

# Print finish message for processing log
echo
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Finished MODIS destriping algorithm at "`date`
#-------------------------------------------------------------------------------

# Exit gracefully
exit(0)
