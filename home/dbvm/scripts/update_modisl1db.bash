#!/bin/bash

# Update MODISL1DB calibration lookup tables
#
# NOTE: This script is intended to run via crontab,
# so the DBVM setup script must be run first (see below).

# Run the DBVM setup script (DBVM_HOME is set in the crontab file)
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$DBVM_HOME/logs/update_modisl1db.log
exec >>$LOG
exec 2>>$LOG
echo "________________________"
echo $0 started at `date -u`

# Set up MODISL1DB
export DBHOME=$DBVM_HOME/apps/modisl1db
source $DBHOME/run/scripts/modisl1db_env.bash

# Update LUTs for Terra
modis_update_luts.csh terra -quiet
if [ $? -eq 0 ]; then
  echo "Terra LUTs updated"
else
  echo "ERROR: Could not update Terra LUTs at "`date -u`
fi

# Update LUTs for Aqua
modis_update_luts.csh aqua -quiet
if [ $? -eq 0 ]; then
  echo "Aqua LUTs updated"
else
  echo "ERROR: Could not update Aqua LUTs at "`date -u`
fi

exit 0
