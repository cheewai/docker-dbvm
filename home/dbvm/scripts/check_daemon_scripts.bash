#!/bin/bash 

# Check to see if all DBVM daemon processing scripts are running.
#
# NOTE: This script is intended to run via crontab,
# so the DBVM setup script must be run first (see below).

#------------------------------------------------------------------------------
# Function to check if a DBVM daemon script is running, and start it if needed. 
function check_daemon_script {
script=$1
pgrep -u $LOGNAME -f $1 &> /dev/null
if [ $? -ne 0 ]; then 
  echo $(date -u) $script is not running - restarting it now
  ($script &)
fi
}
#------------------------------------------------------------------------------

# Run the DBVM setup script (DBVM_HOME is set in the crontab file)
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$LOG_DIR/check_daemon_scripts.log
exec >>$LOG
exec 2>>$LOG

# Check Level 0 event script
# (this should always be running)
check_daemon_script daemon_pds_event.bash

# Check local data ingest script
# (this should be running if Level 0 files data are ingested from a local disk)
if [ "$DBVM_LOCAL_DATA" == "YES" ]; then
  check_daemon_script daemon_get_local_data.bash
fi

# Check FTP data ingest script
# (this should be running if Level 0 files data are ingested from an FTP site)
if [ "$DBVM_FTP_DATA" == "YES" ]; then
  check_daemon_script daemon_get_ftp_data.bash
fi

exit 0
