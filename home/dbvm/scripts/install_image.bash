#!/bin/bash

# Install MODIS Image processing applications

echo
echo "Starting installation of MODIS Image processing applications, please wait..."

echo "(Installing HDFLOOK)"
cd $DBVM_HOME/apps
mkdir hdflook
cd hdflook
tar zxf $DBVM_HOME/tarfiles/LINUX_INTEL32_HDFLook.tar.gz
tar zxf $DBVM_HOME/tarfiles/MAPS.tar.gz

echo "(Installing DBGE)"
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/dbge_v1.2.tar.gz

echo "(Installing DBGE patches)"
rsync -aC $DBVM_HOME/scripts/patches/dbge $DBVM_HOME/apps

echo "Finished installation of MODIS Image processing applications"
exit 0
