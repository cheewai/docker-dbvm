#!/bin/bash
cd $DBVM_HOME/tarfiles
curl -O ftp://ftp.ssec.wisc.edu/pub/IMAPP/MODIS/hidden/modisl2_v2.1/IMAPP_MODISL2_V2.1_MOD06OD_COEFF.tar.gz
cd $DBVM_HOME/apps
tar zxf $DBVM_HOME/tarfiles/IMAPP_MODISL2_V2.1_MOD06OD_COEFF.tar.gz
