#!/bin/bash

# Script for MODIS Google Earth processing
# Liam.Gumley@ssec.wisc.edu
# 06-MAY-2009

echo
echo "Creating MODIS Google Earth products; started at "$(date -u)

#-------------------------------------------------------------------------------
# Check arguments
#-------------------------------------------------------------------------------
# Check input arguments
if [ $# -ne 1 ]; then
  echo "Usage: run_modis_dbge.bash file_1km "
  echo "where"
  echo "file_1km is the full path and name of the input MODIS Level 1B 1KM HDF file"
  exit 1
fi

# Get input L1B 1KM file name and directory
file_1km=$1
dir_1km=$(dirname $file_1km)
echo
echo "Input Level 1B 1KM file: "$file_1km

# Check for input L1B 1KM file
if [ ! -r $file_1km ]; then
  echo "Input L1B 1KM file not found: "$file_1km
  exit 1
fi

# Get satellite/date/time (e.g., a1.06260.2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
date_time=$(basename $file_1km | cut -d. -f1-3)

# Get pass year, day of year, time (e.g, 06 260 2057) from L1B 1KM file name (e.g., a1.06260.2057.1000m.hdf)
year=$(basename $file_1km | cut -c4-5)
jday=$(basename $file_1km | cut -c6-8)
time=$(basename $file_1km | cut -c10-13)

# Get HKM, QKM, and GEO file names
file_hkm=$dir_1km/$date_time.500m.hdf
file_qkm=$dir_1km/$date_time.250m.hdf
file_geo=$dir_1km/$date_time.geo.hdf

#-------------------------------------------------------------------------------
# CHECK FOR DAY MODE
#-------------------------------------------------------------------------------
echo
echo "(Checking for daytime mode)"
# If daytime data was not found, then exit
if [ ! -r $file_hkm ]; then
  echo "MODIS L1B HKM data was not found; daytime products will not be generated"
  echo "Finished MODIS Google Earth products at "$(date -u)
  exit 0
else
  echo "MODIS L1B HKM data was found; daytime products will be generated"
fi

# Update logo file modification time so the scrub script does not delete it
logo_file=$DATA_DIR/kml/logo.jpg
if [ -r $$logo_file ]; then touch $logo_file; fi

#-------------------------------------------------------------------------------
# Run DBGE Google Earth processing
#-------------------------------------------------------------------------------
echo
echo "(Starting DBGE Google Earth processing)"
export DBGE_HOME=$DBVM_HOME/apps/dbge
source $DBVM_HOME/scripts/env_dbge.bash
sat_id=$(basename $file_1km | cut -c1-2)
if [ "$sat_id" == "t1" ]; then sat_name="terra"; else sat_name="aqua"; fi
echo process_granule.bash $file_1km $file_hkm $file_qkm $file_geo $sat_name 20$year$jday $time
process_granule.bash $file_1km $file_hkm $file_qkm $file_geo $sat_name 20$year$jday $time
if [ $? -ne 0 ]; then
  echo "DBGE processing failed for input file: "$file_1km
  exit 1
fi
echo "(Finishing DBGE Google Earth processing)"

#-------------------------------------------------------------------------------
# EXIT
#-------------------------------------------------------------------------------
echo
echo "Finished MODIS Google Earth products at "$(date -u)
exit 0
