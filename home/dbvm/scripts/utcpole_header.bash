#!/bin/bash
# Print a valid utcpole.dat header line for the current time
date1="$(date -u +'%Y-%m-%dT%H:%M:%SZ')"
date2="$(date -u +'%b  %d' -d '1 day ago')"
echo "File Updated: $date1, using USNO ser7 finals.data file of $date2"
echo "MJD	x(arc sec)	x error		y(arc sec)	y error		UT1-UTC(s)	UT error	qual"
exit 0
