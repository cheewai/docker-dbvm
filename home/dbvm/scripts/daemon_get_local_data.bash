#!/bin/bash

# Script to ingest Level 0 MODIS data files from a local directory (e.g., an RT-STPS server)

# NOTES:
# 1. This script is intended to run continuously in the background, i.e., $ daemon_get_local_data.bash &
# 2. This script must be restarted if it is changed
# 3. To restart the script, execute restart_daemon_scripts.bash

# Run the DBVM setup script
if [ -z "$DBVM_HOME" ]; then echo 'ERROR: $DBVM_HOME is not set'; exit 1; fi
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$LOG_DIR/daemon_get_local_data.log
exec >>$LOG
exec 2>>$LOG

# Set local Level 0 directory name 
source_terra_dir=$DBVM_LOCAL_DIR
source_aqua_dir=$DBVM_LOCAL_DIR
source_gbad_dir=$DBVM_LOCAL_DIR
source_airs_dir=$DBVM_LOCAL_DIR

# Set local target directory name
target_dir=$L0_DIR

# Start infinite loop
while [ true ]; do

  # Get MODIS Level 0 construction record (CR) files (P???0064*00.PDS)
  # that were modified in the last 120 minutes
  find $source_terra_dir/ -name "P0420064*00.PDS" -mmin -120 -exec rsync -aq {} $target_dir \; &> /dev/null
  find $source_aqua_dir/  -name "P1540064*00.PDS" -mmin -120 -exec rsync -aq {} $target_dir \; &> /dev/null
  cr_list=$(find $target_dir/ -name "P???0064*00.PDS" -mmin -120 | sort)

  # Loop over CR files
  for cr_file in $cr_list; do

    # Get MODIS Level 0 data file name (P???0064*01.PDS)
    cr_base=$(basename $cr_file)
    data_file=${cr_base%00.PDS}"01.PDS"
    sat_id=$(echo $data_file | cut -c1-4)
    
    # Get MODIS Level 0 data file if we don't have it already and create event file
    if [ ! -e $target_dir/$data_file ]; then

      echo "Ingesting "$data_file" from "$source_terra_dir" at "$(date)

      if [ "$sat_id" == "P042" ]; then

        # Get Terra MODIS Level 0 file
        rsync -aq $source_terra_dir/$data_file $target_dir

      else
      
        # Get Aqua MODIS Level 0 file
        rsync -aq $source_aqua_dir/$data_file $target_dir

        # Get Aqua GBAD Level 0 data file
        gbad_file="P1540957"$(echo $data_file | cut -c9-)
        rsync -aq $source_gbad_dir/$gbad_file $target_dir

        # Get AIRS, AMSU, AMSR-E, HSB Level 0 data files
        apid_list="261 262 290 342 402 404 405 406 407 414 415 957"
        for apid in $apid_list; do
          airs_file=$(echo $data_file | sed 's/P1540064/P1540'$apid'/')
          rsync -aq $source_airs_dir/$airs_file $target_dir
        done

      fi

      # Create the event file
      touch $target_dir/$data_file.event

    fi

  done

  # Wait before checking again
  sleep $DBVM_LOCAL_INTERVAL

done

exit 0
