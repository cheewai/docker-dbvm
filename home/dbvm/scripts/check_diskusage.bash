#!/bin/bash

# Script to check disk usage
#
#
# NOTE: This script is intended to run via crontab,
# so the DBVM setup script must be run first (see below).

# Run the DBVM setup script (DBVM_HOME is set in the crontab file)
source $DBVM_HOME/scripts/dbvm_env.bash

# Set up logfile
LOG=$DBVM_HOME/logs/check_diskusage.log
exec >>$LOG
exec 2>>$LOG
echo "________________________"
echo $0 started at `date -u`

# Check disk usage on specified directories
dir=$DBVM_HOME
df -P $dir | gawk '{if ($5 + 0 > 95) print "ERROR: Disk usage exceeds 95% on "$6}'
dir=$DATA_HOME
df -P $dir | gawk '{if ($5 + 0 > 95) print "ERROR: Disk usage exceeds 95% on "$6}'
dir=$WORK_HOME
df -P $dir | gawk '{if ($5 + 0 > 95) print "ERROR: Disk usage exceeds 95% on "$6}'

exit 0
